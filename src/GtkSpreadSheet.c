#include "GtkSpreadSheet.h"

/**
 * SECTION:GtkSpreadSheet
 * @Short_description: a little spread sheet
 * @Title: GtkSpreadSheet
 *
 */

G_DEFINE_TYPE_WITH_PRIVATE (GtkSpreadSheet, gtk_spread_sheet, GTK_TYPE_DRAWING_AREA)

static void gtk_spread_sheet_class_init (GtkSpreadSheetClass *class);
static void gtk_spread_sheet_init (GtkSpreadSheet *spreadsheet);
static void gtk_spread_sheet_finalize (GObject * object); // Destructeur

/* Renvoie la largeur ou la hauteur minimale d'une colonne ou d'une ligne
 * en fonction du contenu des cellules. Permet d'interdire une taille
 * graphique inférieure au texte inséré.
 *
 * colrow : n° de colonne ou de ligne à déterminer
 * move : calcul sur la colonne ou sur la ligne 
 */
static gint gtk_spread_sheet_get_max_col_row_text_size (GtkSpreadSheet *spreadsheet, gint colrow, TYPE_OF_MOVE move);

/* Redimensionne toutes les cellules de la table */
static void gtk_spread_sheet_tab_reorder (GtkSpreadSheet *spreadsheet);

/* Colore une ligne ou une colonne entière */
static void gtk_spread_sheet_rows_cols_color (GtkSpreadSheet *spreadsheet, gint coordi, gint coordj);

/* Initialise le tableur en créant le nombre de cellules nécessaires */
static void gtk_spread_sheet_tab_init (GtkSpreadSheet *spreadsheet, gint width, gint height);

/* Retourne TRUE si le point (x,y) est inclus dans le rectangle défini par first et last.
 * first et last peuvent ne pas être dans l'ordre (haut/gauche et bas/droit). */
static gboolean gtk_spread_sheet_point_in_rectangle (GdkPoint first, GdkPoint last, gint x, gint y);

/* Remet dans l'ordre les coordonnées haut/gauche et bas/droit pour une sélection multiple */
static void gtk_spread_sheet_reorder_multiple_selected (GtkSpreadSheet *spreadsheet);

/* Callback de mise à jour de l'affichage */
static gboolean gtk_spread_sheet_callback_expose_tab (GtkWidget *spreadsheet, GdkEvent *event, gpointer p);

/* Renvoie les coordonnées tableur de la cellule actuellement pointée.
 * renvoie -1, -1 si aucune cellule est pointée.
 */
static void gtk_spread_sheet_cell_under_mouse (GtkSpreadSheet *spreadsheet, gint mousex, gint mousey, gint *cellx, gint *celly);

/* Dimensionne les colonnes ou les lignes. */
static void gtk_spread_sheet_rows_cols_size_changed (GtkSpreadSheet *spreadsheet, gint coordi, gint coordj, gint move);

/* Gestion principale du mouvement de la souris sur le tableur */
static gboolean gtk_spread_sheet_callback_motion (GtkWidget *spreadsheet, GdkEventMotion *ev, gpointer user_data);

/* Gestion du double clic gauche */
static void callback_double_clicked (GtkSpreadSheet *spreadsheet, GtkSpreadSheetCell *cell);

/* Sous fonction pour gérer l'édition d'une cellule */
static gboolean gtk_spread_sheet_edit_cell (GtkSpreadSheet *spreadsheet, gint x, gint y);

/* Gestion du double clic gauche sur une cellule */
static gboolean gtk_spread_sheet_callback_double_left_clicked_event (GtkWidget *spreadsheet, GdkEventButton *ev, gpointer user_data);

/* Gestion du clic gauche sur une cellule */
static gboolean gtk_spread_sheet_callback_left_clicked_event (GtkWidget *spreadsheet, GdkEventButton *ev, gpointer user_data);

/* Gestion du clic droit pour affichage du menu contextuel */
static gboolean gtk_spread_sheet_callback_right_clicked_event (GtkWidget *spreadsheet, GdkEventButton *ev, gpointer user_data);

/* Gestion principale des clics de la souris sur le tableur. */
static gboolean gtk_spread_sheet_callback_press_event (GtkWidget *spreadsheet, GdkEventButton *ev, gpointer user_data);

/* Gestion principale des relachés des boutons de la souris sur le tableur */
static gboolean gtk_spread_sheet_callback_release_event (GtkWidget *spreadsheet, GdkEventButton *ev, gpointer user_data);

static guint gtk_spread_sheet_signals [GTK_SPREAD_SHEET_NB_SIGNALS] = { 0 };

#define GET_CELL(sheet, i, j) (gtk_spread_sheet_get_cell (sheet, i, j))

static void
gtk_spread_sheet_class_init (GtkSpreadSheetClass *class)
{
  g_return_if_fail (class != NULL);

  G_OBJECT_CLASS(class)->finalize = gtk_spread_sheet_finalize; // destructeur

  /**
   * GtkSpreadSheet::changed:
   * @spreadsheet: le widget qui recoit ce signal
   * @cell: la cellule qui & été modifiée
   * @user_data: données utilisateur
   *
   * Le signal ::changed est émis par défaut à chaque fois qu'une cellule est
   * finie d'être éditée en utilisant la touche "Entrer" du clavier.
   */
  gtk_spread_sheet_signals [GTK_SPREAD_SHEET_CHANGED_SIGNAL] =
    g_signal_new ("changed",
                  G_TYPE_FROM_CLASS (class),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (GtkSpreadSheetClass, changed),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__POINTER,
                  G_TYPE_NONE,
                  1, G_TYPE_POINTER);

  gtk_spread_sheet_signals [GTK_SPREAD_SHEET_SELECT_COLUMN_SIGNAL] =
    g_signal_new ("select_column",
                  G_TYPE_FROM_CLASS (class),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (GtkSpreadSheetClass, select_column),
                  NULL, NULL,
                  g_cclosure_user_marshal_VOID__POINTER_POINTER,
                  G_TYPE_NONE,
                  2, G_TYPE_POINTER, G_TYPE_POINTER);

  gtk_spread_sheet_signals [GTK_SPREAD_SHEET_SELECT_ROW_SIGNAL] =
    g_signal_new ("select_row",
		  G_TYPE_FROM_CLASS (class),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GtkSpreadSheetClass, select_row),
		  NULL, NULL,
		  g_cclosure_user_marshal_VOID__POINTER_POINTER,
		  G_TYPE_NONE,
		  2, G_TYPE_POINTER, G_TYPE_POINTER);

  gtk_spread_sheet_signals [GTK_SPREAD_SHEET_SELECT_RANGE_SIGNAL] =
    g_signal_new ("select_range",
		  G_TYPE_FROM_CLASS (class),
		  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (GtkSpreadSheetClass, select_range),
                  NULL, NULL,
                  g_cclosure_user_marshal_VOID__POINTER_POINTER,
                  G_TYPE_NONE,
                  2, G_TYPE_POINTER, G_TYPE_POINTER);

  gtk_spread_sheet_signals [GTK_SPREAD_SHEET_SELECT_MOVE_SIGNAL] =
    g_signal_new ("select_move",
		  G_TYPE_FROM_CLASS (class),
		  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (GtkSpreadSheetClass, select_move),
                  NULL, NULL,
                  g_cclosure_user_marshal_VOID__POINTER_POINTER,
                  G_TYPE_NONE,
                  2, G_TYPE_POINTER, G_TYPE_POINTER);
}

static void
gtk_spread_sheet_init (GtkSpreadSheet *spreadsheet)
{
  g_return_if_fail (spreadsheet != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  priv->nb_rows = 1;
  priv->nb_cols = 1;
  priv->colcursor = NULL;
  priv->rowcursor = NULL;
  priv->TYPE_REDIM = NONE;
  priv->increment = AUTO_INCREMENT;
  priv->COL_ROW_RESIZE = FALSE;
  priv->MULTIPLE_SELECT = FALSE;
  priv->rootwindow = NULL;
  priv->winentry = NULL;
  priv->emitted_signal = NONE;
  priv->lastcellchanged = NULL;
  priv->cellunderpointer = NULL;
  priv->selecttopleftcell = NULL;
  priv->selectbottomrightcell = NULL;
  priv->clipboard = NULL;
  priv->contextmenu = NULL;
  priv->cutitem = NULL;
  priv->copyitem = NULL;
  priv->pasteitem = NULL;
  priv->selected_enter_move = GTK_SPREAD_SELECT_MOVE_DOWN;
}

static void
gtk_spread_sheet_finalize (GObject * object)
{
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (GTK_SPREAD_SHEET (object));

  /* Destruction de la hashtable */
  g_hash_table_destroy (priv->tab);
  priv->tab = NULL;

  /* Destrcution des images de la souris */
  gdk_cursor_unref (priv->colcursor);
  gdk_cursor_unref (priv->rowcursor);

  /* Destruction du clipboard interne */
  g_object_unref (priv->clipboard);

  G_OBJECT_CLASS (gtk_spread_sheet_parent_class)->finalize (object);
}

/******************************************************************************/

static gint
gtk_spread_sheet_get_max_col_row_text_size (GtkSpreadSheet *spreadsheet, gint colrow, TYPE_OF_MOVE move)
{
  g_return_val_if_fail (spreadsheet != NULL, -1);
  g_return_val_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet), -1);
  g_return_val_if_fail (colrow>-1, -1);

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  switch (move)
    {
    case COLUMNS:
      {
	g_return_val_if_fail (colrow<priv->nb_cols, -1);
	break;
      }
    case ROWS:
      {
	g_return_val_if_fail (colrow<priv->nb_rows, -1);
	break;
      }

    default:
      return -1;
    }

  cairo_t *cr = gdk_cairo_create (gtk_widget_get_window (GTK_WIDGET(spreadsheet)));
  gint i, j;
  gchar *key = NULL;
  GtkSpreadSheetCell *cell = NULL;
  cairo_text_extents_t extents;

  cairo_set_font_size (cr, 15);
  switch (move)
    {
    case COLUMNS :
      {
	gint maxwidth = 0;
	for (j=0; j<priv->nb_rows; j++)
	  {
	    key = g_strdup_printf ("%d%d", colrow, j);
	    cell = GET_CELL (GTK_SPREAD_SHEET (spreadsheet), colrow, j);
	    g_free (key);
	  
	    /* Récupération de la taille graphique du texte de la cellule */
	    cairo_text_extents (cr, gtk_spread_sheet_cell_get_text (cell), &extents);

	    if (extents.width>maxwidth)
	      maxwidth = extents.width;
	  }

	cairo_destroy (cr);

	return maxwidth+10;
	break;
      }

    case ROWS :
      {
	gint maxheight = 0;
	for (i=0; i<priv->nb_cols; i++)
	  {
	    key = g_strdup_printf ("%d%d", i, colrow);
	    cell = GET_CELL (GTK_SPREAD_SHEET (spreadsheet), i, colrow);
	    g_free (key);
	  
	    /* Récupération de la taille graphique du texte de la cellule */
	    cairo_text_extents (cr, gtk_spread_sheet_cell_get_text (cell), &extents);

	    if (extents.height>maxheight)
	      maxheight = extents.height;
	  }

	cairo_destroy (cr);
	
	return maxheight+10;
	break;
      }

    default:
      {
	return -1;
      }
    }

  cairo_destroy (cr);
  
  return -1;
}

static void
gtk_spread_sheet_tab_reorder (GtkSpreadSheet *spreadsheet)
{
  gint i, j;
  GdkPoint offset; // position courante pour calculer la position de la cellule suivante
  offset.x = 0;
  offset.y = 0;

  g_return_if_fail (spreadsheet != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));
  
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);
  GtkSpreadSheetCell *cell = NULL;
  gint posx, posy;
  gint width, height;

  for (j=0; j<priv->nb_rows; j++)
    {
      for (i=0; i<priv->nb_cols; i++)
	{
	  cell = GET_CELL (spreadsheet, i, j);
	  gtk_spread_sheet_cell_get_graphic_size (cell, &width, &height);

	  gtk_spread_sheet_cell_set_graphic_position (cell, offset.x, offset.y);
	  
	  offset.x += width;
	}
      offset.x = 0;
      cell = GET_CELL (spreadsheet, i-1, j);
      gtk_spread_sheet_cell_get_graphic_size (cell, &width, &height);
      offset.y += height;
    }
}

static void
gtk_spread_sheet_rows_cols_color (GtkSpreadSheet *spreadsheet, gint coordi, gint coordj)
{
  g_return_if_fail (spreadsheet != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));
  
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);
  
  cairo_t *cr = gdk_cairo_create (gtk_widget_get_window (GTK_WIDGET (spreadsheet)));
  GtkSpreadSheetCell *cell = GET_CELL (spreadsheet, coordi, coordj);
  gint posx, posy;
  gint width, height;
  GtkAllocation alloc;
  
  gtk_widget_get_allocation (GTK_WIDGET (spreadsheet), &alloc);
  gtk_spread_sheet_cell_get_graphic_position (cell, &posx, &posy);
  gtk_spread_sheet_cell_get_graphic_size (cell, &width, &height);

  cairo_set_source_rgba (cr, 0, 1, 0, 0.2);
  switch (priv->TYPE_REDIM)
    {
    case COLUMNS :
      {
	cairo_rectangle (cr, posx, 0, width, alloc.height);
	break;
      }

    case ROWS :
      {
	cairo_rectangle (cr, 0, posy, alloc.width, height);
      }
    }
  
  cairo_fill (cr);
  cairo_destroy (cr);
}

static void
gtk_spread_sheet_screen_size_changed (GtkSpreadSheet *spreadsheet)
{
  g_return_if_fail (spreadsheet != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));
  
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);
  GtkSpreadSheetCell *cell = NULL;
  gint cellsize;
  gint i=0;
  gint width=0, height = 0;

  for (i=0; i<priv->nb_cols; i++)
    {
      cell = GET_CELL (spreadsheet, i, 0);
      gtk_spread_sheet_cell_get_graphic_size (cell, &cellsize, NULL);
      width += cellsize;
    }

  for (i=0; i<priv->nb_rows; i++)
    {
      cell = GET_CELL (spreadsheet, 0, i);
      gtk_spread_sheet_cell_get_graphic_size (cell, NULL, &cellsize);
      height += cellsize;
    }

  gtk_widget_set_size_request (GTK_WIDGET (spreadsheet), width, height);
}

static gboolean
gtk_spread_sheet_hashtable_equal_func (gconstpointer a, gconstpointer b)
{
  if (g_strcmp0 (a, b)==0)
    return TRUE;
  
  return FALSE;
}

static void
gtk_spread_sheet_hashtable_key_destroy (gchar *key)
{
  g_free (key);
}

static void
gtk_spread_sheet_hashtable_value_destroy (GtkSpreadSheetCell *cell)
{
  g_object_unref (cell);
}

void
gtk_spread_sheet_insert_col_row (GtkSpreadSheet *spreadsheet, gint num, TYPE_OF_MOVE colrow, gboolean after)
{
  g_return_if_fail (spreadsheet != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);
  g_return_if_fail (colrow==ROWS || colrow==COLUMNS);
  switch (colrow)
    {
    case ROWS:
      if (num<priv->nb_rows && after) num++; 
      if (num>priv->nb_rows) num=priv->nb_rows;
      break;
    case COLUMNS:
      if (num<priv->nb_cols && after) num++;
      if (num>priv->nb_cols) num=priv->nb_cols;
      break;
    } 

  gchar *key = NULL;
  GtkSpreadSheetCell *cell = NULL;

  switch (colrow)
    {
    case COLUMNS:
      {
	gint i, j;
	gint posx, posy;
	gint width, height;
	GtkSpreadSheetCell *orgcell=NULL;
	priv->nb_cols++;

	for (j=0; j<priv->nb_rows; j++)
	  {
	    key = g_strdup_printf ("c%dr%d", priv->nb_cols-1, j);

	    if (j==0)
	      cell = gtk_spread_sheet_cell_new (BORDER, priv->nb_cols-1, j, NULL);
	    else
	      cell = gtk_spread_sheet_cell_new (NORMAL, priv->nb_cols-1, j, NULL);

	    /* Dimensionnement de la nouvelle cellule et positionnement dans l'espace graphique */
	    /* En premier lieu récupération de la cellule située en extrémité du tableur à la même ligne.
	     * Cette cellule va permettre de calculer la position de la nouvelle cellule à insérer.
	     */
	    orgcell = gtk_spread_sheet_get_cell (spreadsheet, priv->nb_cols-2, j);
	    gtk_spread_sheet_cell_get_graphic_position (orgcell, &posx, &posy);
	    gtk_spread_sheet_cell_get_graphic_size (orgcell, &width, &height);

	    /* Positionnement de la nouvelle cellule. */
	    gtk_spread_sheet_cell_set_graphic_position (cell, posx+width, posy);
	    gtk_spread_sheet_cell_set_graphic_size (cell, width, height);

	    /* Sauvegarde de la cellule dans la hashtable */
	    g_hash_table_insert (priv->tab, key, cell);
	  }

	/* Décalage du contenu de toutes les cellules vers la droite depuis la colonne sélectionnée en tenant compte de leur
	 * taille respective. */
	for (i=priv->nb_cols-1; i>num; i--)
	  {
	    for (j=0; j<priv->nb_rows; j++)
	      {
		cell = gtk_spread_sheet_get_cell (spreadsheet, i, j);
		orgcell = gtk_spread_sheet_get_cell (spreadsheet, i-1, j);
		gtk_spread_sheet_cell_set_text (cell, gtk_spread_sheet_cell_get_text(orgcell));
	      }
	  }

	/* Effacement du contenu de la colonne sélectionnée. */
	for (j=0; j < priv->nb_rows; j++)
	  {
	    cell = gtk_spread_sheet_get_cell (spreadsheet, num, j);
	    gtk_spread_sheet_cell_set_text (cell, "");
	  }
	
	/* Redéfinition des labels si AUTO_INCREMENT est défini */
	if (priv->increment==AUTO_INCREMENT)
	  {
	    gchar *title = NULL;
	    for (i=1; i < priv->nb_cols; i++)
	      {
		cell = gtk_spread_sheet_get_cell (spreadsheet, i, 0);
		title = g_strdup_printf ("%d", i);
		gtk_spread_sheet_cell_set_text (cell, title);
		g_free (title);
	      }
	  }

	/* Calcul de la nouvelle taille de l'espace graphique */
	gtk_spread_sheet_screen_size_changed (spreadsheet);

	break;
      }
    case ROWS:
      {
	gint i, j;
	gint posx, posy;
	gint width, height;
	GtkSpreadSheetCell *orgcell=NULL;
	priv->nb_rows++;
  
	for (i=0; i<priv->nb_cols; i++)
	  {
	    key = g_strdup_printf ("c%dr%d", i, priv->nb_rows-1);

	    if (i==0)
	      cell = gtk_spread_sheet_cell_new (BORDER, i, priv->nb_rows-1, NULL);
	    else
	      cell = gtk_spread_sheet_cell_new (NORMAL, i, priv->nb_rows-1, NULL);

	    /* Dimensionnement de la nouvelle cellule et positionnement dans l'espace graphique */
	    /* En premier lieu récupération de la cellule située en extrémité du tableur à la même colonne.
	     * Cette cellule va permettre de calculer la position de la nouvelle cellule à insérer.
	     */
	    orgcell = gtk_spread_sheet_get_cell (spreadsheet, i, priv->nb_rows-2);
	    gtk_spread_sheet_cell_get_graphic_position (orgcell, &posx, &posy);
	    gtk_spread_sheet_cell_get_graphic_size (orgcell, &width, &height);

	    /* Positionnement de la nouvelle cellule. */
	    gtk_spread_sheet_cell_set_graphic_position (cell, posx, posy+height);
	    gtk_spread_sheet_cell_set_graphic_size (cell, width, height);

	    /* Sauvegarde de la cellule dans la hashtable */
	    g_hash_table_insert (priv->tab, key, cell);
	  }

	/* Décalage du contenu de toutes les cellules vers le bas depuis la ligne sélectionnée en tenant compte de leur
	 * taille respective. */
	for (j=priv->nb_rows-1; j>num; j--)
	  {
	    for (i=0; i<priv->nb_cols; i++)
	      {
		cell = gtk_spread_sheet_get_cell (spreadsheet, i, j);
		orgcell = gtk_spread_sheet_get_cell (spreadsheet, i, j-1);
		gtk_spread_sheet_cell_set_text (cell, gtk_spread_sheet_cell_get_text(orgcell));
	      }
	  }

	/* Effacement du contenu de la colonne sélectionnée. */
	for (i=0; i < priv->nb_cols; i++)
	  {
	    cell = gtk_spread_sheet_get_cell (spreadsheet, i, num);
	    gtk_spread_sheet_cell_set_text (cell, "");
	  }
	
	/* Redéfinition des labels si AUTO_INCREMENT est défini */
	if (priv->increment==AUTO_INCREMENT)
	  {
	    gchar *title = NULL;
	    for (j=1; j < priv->nb_rows; j++)
	      {
		cell = gtk_spread_sheet_get_cell (spreadsheet, 0, j);
		title = g_strdup_printf ("%d", j);
		gtk_spread_sheet_cell_set_text (cell, title);
		g_free (title);
	      }
	  }
      }
      
      /* Calcul de la nouvelle taille de l'espace graphique */
      gtk_spread_sheet_screen_size_changed (spreadsheet);
    }
}

void
gtk_spread_sheet_remove_col_row (GtkSpreadSheet *spreadsheet, gint num, TYPE_OF_MOVE colrow)
{
  g_return_if_fail (spreadsheet != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);
  g_return_if_fail (colrow==ROWS || colrow==COLUMNS);

  /* Interdiction de supprimer la première ligne ou la première colonne qui
  * contient les labels */
  g_return_if_fail (num>0);

  switch (colrow)
    {
    case ROWS:
      g_return_if_fail (priv->nb_rows>2);
      g_return_if_fail (num<=priv->nb_rows);
      break;
    case COLUMNS:
      g_return_if_fail (priv->nb_cols>2);
      g_return_if_fail (num<=priv->nb_cols);
      break;
    }

  gchar *key = NULL;
  GtkSpreadSheetCell *cell = NULL;

  switch (colrow)
    {
    case COLUMNS:
      {
	gint i, j;
	gint width, height;
	gint posx, posy;
	GtkSpreadSheetCell *orgcell=NULL;

	/* Si ce n'est pas la dernière colonne qui doit être supprimée on décale
	 * toutes les colonnes */
	if (num<priv->nb_cols-1)
	  {
	    /* Ce n'est pas la dernière colonne. Décalage du contenu de toutes les
	     * cellules vers la gauche depuis la colonne sélectionnée en tenant
	     * compte de leur taille respective. */
	    for (i=num+1; i<priv->nb_cols;  i++)
	      {
		for (j=0; j<priv->nb_rows; j++)
		  {
		    cell = gtk_spread_sheet_get_cell (spreadsheet, i-1, j);
		    orgcell = gtk_spread_sheet_get_cell (spreadsheet, i, j);
		
		    gtk_spread_sheet_cell_get_graphic_size (orgcell, &width, &height);
		    gtk_spread_sheet_cell_set_graphic_size (cell, width, height);
		
		    gtk_spread_sheet_cell_get_graphic_position (cell, &posx, &posy);
		    gtk_spread_sheet_cell_set_graphic_position (orgcell, posx+width, posy);

		    gtk_spread_sheet_cell_set_text (cell, gtk_spread_sheet_cell_get_text(orgcell));
		  }
	      }
	  }

	/* Suppression des cellules de la dernière colonne dans la table de hash */
	gchar *key=NULL;
	for (j=0; j<priv->nb_rows; j++)
	  {
	    key = g_strdup_printf ("c%dr%d", priv->nb_cols-1, j);
	    g_hash_table_remove (priv->tab, key);
	    g_free (key);
	  }

	/* Décrémentation du nombre de colonnes. */
	priv->nb_cols--;

	/* Calcul de la nouvelle taille de l'espace graphique */
	gtk_spread_sheet_screen_size_changed (spreadsheet);

	/* Suppression de la sélection courante qui n'est plus valable */
	gtk_spread_sheet_deselect (spreadsheet);
	
	break;
      }
    case ROWS:
      {
	gint i, j;
	gint width, height;
	gint posx, posy;
	GtkSpreadSheetCell *orgcell=NULL;

	/* Si ce n'est pas la dernière ligne qui doit être supprimée on décale
	 * toutes les lignes */
	if (num<priv->nb_rows-1)
	  {
	    /* Décalage du contenu de toutes les cellules vers le haut depuis
	     * la ligne sélectionnée en tenant compte de leur taille respective.
	     */
	    for (i=0; i<priv->nb_cols;  i++)
	      {
		for (j=num+1; j<priv->nb_rows; j++)
		  {
		    cell = gtk_spread_sheet_get_cell (spreadsheet, i, j-1);
		    orgcell = gtk_spread_sheet_get_cell (spreadsheet, i, j);
		
		    gtk_spread_sheet_cell_get_graphic_size (orgcell, &width, &height);
		    gtk_spread_sheet_cell_set_graphic_size (cell, width, height);

		    gtk_spread_sheet_cell_get_graphic_position (cell, &posx, &posy);
		    gtk_spread_sheet_cell_set_graphic_position (orgcell, posx, posy+height);
		
		    gtk_spread_sheet_cell_set_text (cell, gtk_spread_sheet_cell_get_text(orgcell));
		  }
	      }
	  }

	/* Suppression des cellules de la dernière ligne dans la table de hash */
	gchar *key=NULL;
	for (i=0; i<priv->nb_cols; i++)
	  {
	    key = g_strdup_printf ("c%dr%d", i, priv->nb_rows-1);
	    g_hash_table_remove (priv->tab, key);
	    g_free (key);
	  }

	/* Décrémentation du nombre de lignes. */
	priv->nb_rows--;

	/* Suppression de la sélection courante qui n'est plus valable */
	gtk_spread_sheet_deselect (spreadsheet);

	/* Calcul de la nouvelle taille de l'espace graphique */
	gtk_spread_sheet_screen_size_changed (spreadsheet);
      }
    }
}

static void
gtk_spread_sheet_tab_init (GtkSpreadSheet *spreadsheet, gint width, gint height)
{
  g_return_if_fail (width>0);
  g_return_if_fail (height>0);
  g_return_if_fail (spreadsheet != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));
  
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);
  gchar *key = NULL;
  GtkSpreadSheetCell *cell = NULL;
  gint i, j;
  GdkPoint offset;

  priv->nb_cols = width;
  priv->nb_rows = height;

  priv->tab = g_hash_table_new_full (g_str_hash,
				     (GEqualFunc) gtk_spread_sheet_hashtable_equal_func,
				     (GDestroyNotify)gtk_spread_sheet_hashtable_key_destroy,
				     (GDestroyNotify)gtk_spread_sheet_hashtable_value_destroy);

  offset.x = 0;
  offset.y = 0;
  for (j=0; j<height; j++)
    {
      for (i=0; i<width; i++)
	{
	  key = g_strdup_printf ("c%dr%d", i, j);
	  if (i==0 || j==0)
	    {
	      if (i==0 && j==0)
		cell = gtk_spread_sheet_cell_new (BORDER, i, j, NULL);
	      else
		{
		  gchar *text = NULL;
		  if (i==0)
		    text = g_strdup_printf ("%d", j);
		  else
		    text = g_strdup_printf ("%d", i);

		  cell = gtk_spread_sheet_cell_new (BORDER, i, j, text);
		  g_free (text);
		}
	    }
	  else
	    cell = gtk_spread_sheet_cell_new (NORMAL, i, j, NULL);

	  gtk_spread_sheet_cell_set_graphic_position (cell, offset.x, offset.y);
	  gtk_spread_sheet_cell_set_graphic_size (cell, 70, 20);

	  offset.x += 70;
	  
	  /* Sauvegarde de la cellule dans la hashtable */
	  g_hash_table_insert (priv->tab,key, cell);
	}
      offset.x = 0;
      offset.y += 20;
    }
  
  /* Calcul de la nouvelle taille de l'espace graphique */
  gtk_spread_sheet_screen_size_changed (spreadsheet);

  /* initialisation de la cellule actuellement pointée par la souris.
   * Permet de mettre à jour la cellule lorsque la souris la quitte.
   * À -1 aucune cellule n'est pointée par la souris.
   */
  priv->activecell.x = -1;
  priv->activecell.y = -1;
}

static gboolean
gtk_spread_sheet_point_in_rectangle (GdkPoint first, GdkPoint last, gint x, gint y)
{
  GdkRectangle rect;

  if (first.x>last.x)
    {
      rect.x = last.x;
      rect.width = first.x;
    }
  else
    {
      rect.x = first.x;
      rect.width = last.x;
    }

  if (first.y>last.y)
    {
      rect.y = last.y;
      rect.height = first.y;
    }
  else
    {
      rect.y = first.y;
      rect.height = last.y;
    }

  if (x>=rect.x && x<=rect.width &&
      y>=rect.y && y<=rect.height)
    return TRUE;

  return FALSE;
}

static void
gtk_spread_sheet_reorder_multiple_selected (GtkSpreadSheet *spreadsheet)
{
  g_return_if_fail (spreadsheet!=NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  /* S'il n'y a pas de sélection courante on sort */
  if (!priv->selecttopleftcell)
    return;

  GdkPoint topleft, bottomright;
  gtk_spread_sheet_cell_get_spreadsheet_position (priv->selecttopleftcell, &topleft);
  gtk_spread_sheet_cell_get_spreadsheet_position (priv->selectbottomrightcell, &bottomright);

  /* Recherche des minima et maxima sur x et y */
  GdkPoint min, max;
  min.x = priv->nb_cols;
  min.y = priv->nb_rows;
  max.x = 0;
  max.y = 0;
  
  if (topleft.x < min.x) min.x = topleft.x;
  if (bottomright.x < min.x) min.x = bottomright.x;
  if (topleft.y < min.y) min.y = topleft.y;
  if (bottomright.y < min.y) min.y = bottomright.y;

  if (topleft.x > max.x) max.x = topleft.x;
  if (bottomright.x > max.x) max.x = bottomright.x;
  if (topleft.y > max.y) max.y = topleft.y;
  if (bottomright.y > max.y) max.y = bottomright.y;

  priv->selecttopleftcell = GET_CELL (spreadsheet, min.x, min.y);
  priv->selectbottomrightcell = GET_CELL (spreadsheet, max.x, max.y);
}

static gboolean
gtk_spread_sheet_callback_expose_tab (GtkWidget *spreadsheet, GdkEvent *event, gpointer p)
{
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (GTK_SPREAD_SHEET (spreadsheet));
  cairo_t *cr = gdk_cairo_create (gtk_widget_get_window (spreadsheet));
  GtkSpreadSheetCell *cell = NULL;

  cairo_set_source_rgb (cr ,1, 1, 1);
  cairo_paint (cr);

  /* Dessin du tableur */
  gint i, j;
  ColorCell color;
  gint posx, posy;
  gint width, height;
  cairo_text_extents_t extents;

  for (j=0; j<priv->nb_rows; j++)
    {
      for (i=0; i<priv->nb_cols; i++)
	{
	  cell = GET_CELL (GTK_SPREAD_SHEET (spreadsheet), i, j);

	  /* Récupération de la nouvelle taille de la cellule */
	  gtk_spread_sheet_cell_get_graphic_size (cell, &width, &height);
	  /* Récupération de la position de la cellule */
	  gtk_spread_sheet_cell_get_graphic_position (cell, &posx, &posy);
	  /* Dessin du contenu de la cellule si elle existe */
	  /* Vérification si la cellule n'est pas actuellement sélectionnée */
	  if (priv->selecttopleftcell)
	    {
	      GdkPoint topleft, bottomright;
	      gtk_spread_sheet_cell_get_spreadsheet_position (priv->selecttopleftcell, &topleft);
	      gtk_spread_sheet_cell_get_spreadsheet_position (priv->selectbottomrightcell, &bottomright);
	      if (gtk_spread_sheet_point_in_rectangle (topleft, bottomright, i, j))
		{
		  gtk_spread_sheet_cell_paint (cell, cr, i, j, TRUE);
		  /* Dessin du contour */
		  gtk_spread_sheet_cell_get_shapecolor (cell, &color);
		  cairo_set_source_rgb (cr, 1, 1, 1);
		  cairo_set_line_width (cr, 0.5);
		  cairo_rectangle (cr, posx, posy, width, height);
		  cairo_stroke (cr);
		}
	      else
		{
		  gtk_spread_sheet_cell_paint (cell, cr, i, j, FALSE);
		  /* Dessin du contour */
		  gtk_spread_sheet_cell_get_shapecolor (cell, &color);
		  cairo_set_source_rgb (cr, color.red, color.green, color.blue);
		  cairo_set_line_width (cr, 0.2);
		  cairo_rectangle (cr, posx, posy, width, height);
		  cairo_stroke (cr);
		}
	    }
	  else
	    {
	      gtk_spread_sheet_cell_paint (cell, cr, i, j, FALSE);
	      /* Dessin du contour */
	      gtk_spread_sheet_cell_get_shapecolor (cell, &color);
	      cairo_set_source_rgb (cr, color.red, color.green, color.blue);
	      cairo_set_line_width (cr, 0.2);
	      cairo_rectangle (cr, posx, posy, width, height);
	      cairo_stroke (cr);
	    }
	}
    }

  /* Colorisation de la colonne ou de la ligne en vert si un bouton de la souris
   * est actuellement pressé */
  if (priv->COL_ROW_RESIZE)
    gtk_spread_sheet_rows_cols_color (GTK_SPREAD_SHEET (spreadsheet),
				      priv->activecell.x,  priv->activecell.y);
  
  cairo_destroy (cr);

  return FALSE;
}

static void
gtk_spread_sheet_cell_under_mouse (GtkSpreadSheet *spreadsheet, gint mousex, gint mousey, gint *cellx, gint *celly)
{
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  /* Extraction de la cellule actuellement visée */
  GtkSpreadSheetCell *cell = NULL;
  gint posx, posy;
  gint width, height;
  gint i, j;
  *cellx=-1;
  *celly=-1;
  
  for (j=0; j<priv->nb_rows; j++)
    {
      for (i=0; i<priv->nb_cols; i++)
	{
	  cell = GET_CELL (spreadsheet, i, j);
	  gtk_spread_sheet_cell_get_graphic_position (cell, &posx, &posy);
	  gtk_spread_sheet_cell_get_graphic_size (cell, &width, & height);
	  if (mousex > posx && mousex <= posx + width &&
	      mousey > posy && mousey <= posy + height)
	    {
	      *cellx=i; *celly=j;
	      i=priv->nb_cols+1; j=priv->nb_rows+1; // Pour sortir des deux boucles
	    }
	}
    }
}

static void
gtk_spread_sheet_rows_cols_size_changed (GtkSpreadSheet *spreadsheet, gint coordi, gint coordj, gint move)
{
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);
  GtkSpreadSheetCell *cell = GET_CELL (spreadsheet, coordi, coordj);
  gint posx, posy;
  gint width, height;

  gtk_spread_sheet_cell_get_graphic_position (cell, &posx, &posy);

  switch (priv->TYPE_REDIM)
    {
    case COLUMNS :
      {
	gint maxwidth;
	maxwidth = gtk_spread_sheet_get_max_col_row_text_size (spreadsheet, coordi, COLUMNS);
	if (move - posx >= maxwidth)
	  {
	    gint j;
	  
	    for (j=0; j<priv->nb_rows; j++)
	      {
		cell = GET_CELL (spreadsheet, coordi, j);
		gtk_spread_sheet_cell_get_graphic_position (cell, &posx, &posy);
		gtk_spread_sheet_cell_get_graphic_size (cell, &width, &height);

		gtk_spread_sheet_cell_set_graphic_size (cell, move - posx, height);
	      }

	    /* Calibrage de toute la table en fonction de la nouvelle taille */
	    gtk_spread_sheet_tab_reorder (spreadsheet);
	    
	    /* Calcul de la nouvelle taille de l'espace graphique */
	    gtk_spread_sheet_screen_size_changed (spreadsheet);
	  }
	break;
      }

    case ROWS :
      {
	gint maxheight;
	maxheight = gtk_spread_sheet_get_max_col_row_text_size (spreadsheet, coordj, ROWS);
	if (move - posy >= maxheight)
	  {
	    gint i;
	  
	    for (i=0; i<priv->nb_cols; i++)
	      {
		cell = GET_CELL (spreadsheet, i, coordj);
		gtk_spread_sheet_cell_get_graphic_position (cell, &posx, &posy);
		gtk_spread_sheet_cell_get_graphic_size (cell, &width, &height);

		gtk_spread_sheet_cell_set_graphic_size (cell, width, move - posy);
	      }

	    /* Calibrage de toute la table en fonction de la nouvelle taille */
	    gtk_spread_sheet_tab_reorder (spreadsheet);

	    /* Calcul de la nouvelle taille de l'espace graphique */
	    gtk_spread_sheet_screen_size_changed (spreadsheet);
	  }
      }
    }
}

static gboolean
gtk_spread_sheet_callback_motion (GtkWidget *spreadsheet, GdkEventMotion *ev, gpointer user_data)
{
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (GTK_SPREAD_SHEET (spreadsheet));
  gint coordi=-1, coordj=-1; //Coordonnées de la cellule pointée

  /* Si un bouton de la souris est actuellement pressé il faut gérer le
   * redimensionnement de la colonne ou de la ligne.
   */
  if (priv->COL_ROW_RESIZE)
    {
      switch (priv->TYPE_REDIM)
	{
	case COLUMNS:
	  {
	    gtk_spread_sheet_rows_cols_size_changed (GTK_SPREAD_SHEET (spreadsheet),
						     priv->activecell.x, priv->activecell.y,
						     (gint)ev->x);
	    break;
	  }

	case ROWS:
	  {
	    gtk_spread_sheet_rows_cols_size_changed (GTK_SPREAD_SHEET (spreadsheet),
						     priv->activecell.x, priv->activecell.y,
						     (gint)ev->y);
	  }
	}

      /* Sauvegarde de la cellule actuellement pointée */
      gtk_spread_sheet_cell_under_mouse (GTK_SPREAD_SHEET (spreadsheet),
					 ev->x, ev->y, &coordi, &coordj);
      priv->cellunderpointer = GET_CELL (GTK_SPREAD_SHEET (spreadsheet), coordi, coordj);
      
      /* mise à jour graphique */
      gtk_widget_queue_draw (spreadsheet);

      return FALSE;
    }

  /* Extraction de la cellule actuellement visée. Retourne -1 si aucune
   * cellule n'est pointée par la souris.
   * Sauvegarde de celle-ci pour le moteur interne.
   */
  gtk_spread_sheet_cell_under_mouse (GTK_SPREAD_SHEET (spreadsheet),
				     ev->x, ev->y, &coordi, &coordj);
  priv->cellunderpointer = GET_CELL (GTK_SPREAD_SHEET (spreadsheet), coordi, coordj);

  /* Une cellule a été trouvée */
  if (coordi!=-1)
    {
      /* on vérifie si la souris n'a pas quitté une autre cellule
       * avant et si la cellule actuellement pointée n'est pas la même
       * que précédement.
       * Ceci permet de mettre à jour si besoin la cellule précédente
       * et aussi de ne pas forcer une mise à jour de l'affichage si
       * rien n'a changé entre-temps.
       */
      if (priv->activecell.x!=coordi || priv->activecell.y!=coordj)
	{ // Ce n'est pas la même cellule
	  /* Sauvegarde de la cellule pointée */
	  priv->activecell.x = coordi;
	  priv->activecell.y = coordj;

	  /* Si on est en cours de sélection multiple, on sauvegarde la
	   * nouvelle sélection pour permettre un affichage en temps réel
	   * de la sélection.
	   */
	  if (priv->MULTIPLE_SELECT)
	    {
	      /* Vérification si la sélection n'est pas une sélection de
	       * colonnes ou de lignes multiples.
	       */
	      switch (priv->emitted_signal)
		{
		case GTK_SPREAD_SHEET_SELECT_COLUMN_SIGNAL:
		  {
		    if (coordi==0) coordi++;
		    priv->selectbottomrightcell = GET_CELL (GTK_SPREAD_SHEET (spreadsheet), coordi, priv->nb_rows-1);
		    break;
		  }
		case GTK_SPREAD_SHEET_SELECT_ROW_SIGNAL:
		  {
		    if (coordj==0) coordj++;
		    priv->selectbottomrightcell = GET_CELL (GTK_SPREAD_SHEET (spreadsheet), priv->nb_cols-1, coordj);
		    break;
		  }
		default:
		  {
		    priv->selectbottomrightcell = GET_CELL (GTK_SPREAD_SHEET (spreadsheet), coordi, coordj);
		  }
		}
	    }
	      
	  /* Mise à jour de l'affichage */
	  gtk_widget_queue_draw (spreadsheet);
	}
      else /* on est toujours dans la même cellule */
	{
	  /* Vérification si la souris n'est pas sur le bord droit de la
	   * cellule et que la cellule est sur la première ligne
	   */
	  GtkSpreadSheetCell *cell = GET_CELL (GTK_SPREAD_SHEET(spreadsheet), coordi, coordj);
	  gint posx, posy;
	  gint width, height;
	  gtk_spread_sheet_cell_get_graphic_position (cell, &posx, &posy);
	  gtk_spread_sheet_cell_get_graphic_size (cell, &width, &height);
	  if (priv->activecell.y==0 && ev->x>=posx + width-2 && ev->x<=posx + width)
	    {
	      /* Affichage du curseur de redimensionnement horizontal de la cellule . */
	      gdk_window_set_cursor (gtk_widget_get_window (spreadsheet) ,priv->colcursor);
	    }
	  else
	    if (priv->activecell.x==0 && ev->y>=posy + height-2 && ev->y<=posy + height)
	      {
		/* Affichage du curseur de redimensionnement vertical de la cellule . */
		gdk_window_set_cursor (gtk_widget_get_window (spreadsheet) ,priv->rowcursor);
	      }
	    else
	      gdk_window_set_cursor (gtk_widget_get_window (spreadsheet) ,NULL);
	}
    }
  else // Aucune cellule n'a été trouvée. La souris ne pointe plus de cellule
    {
      /* On vérifie si une ancienne cellule était pointée auquel cas
       * on va la remettre dans son état initial. */
      if (priv->activecell.x!=-1 && priv->activecell.y!=-1)
	{
	  /* Suppression de la sauvegarde */
	  priv->activecell.x = -1;
	  priv->activecell.y = -1;

	  /* Affichage du curseur de la souris par défaut */
	  gdk_window_set_cursor (gtk_widget_get_window (spreadsheet) ,NULL);
   
	  /* Mise à jour de l'affichage */
	  gtk_widget_queue_draw (spreadsheet);
	}
    }

  return FALSE;
}

static gboolean
gtk_spread_sheet_edit_cell (GtkSpreadSheet *spreadsheet, gint x, gint y)
{
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);
  gint coordi=-1, coordj=-1;
  GtkSpreadSheetCell *cell = NULL;
  gint posx, posy, width, height;
  GtkAllocation allocation;

  /* Vérification si une cellule n'est pas en cours d'édition. Si c'est le cas
   * suppression de celle-ci. */
  if (priv->winentry)
    {
      gtk_widget_destroy (priv->winentry);
      priv->winentry = NULL;
    }
  
  /* Récupération des coordonnées tableur de la cellule sous le pointeur */
  gtk_spread_sheet_cell_under_mouse (spreadsheet, x, y, &coordi, &coordj);
  /* Si ca ne pointe pas sur une cellule on sort */
  if (coordi==-1 || coordj==-1)
    return FALSE;

  /* Si on essaye d'éditer la colonne 0 ou la ligne 0 on sort */
  if (coordi==0 || coordj==0)
    return FALSE;

  /* Récupération de la cellule */
  cell = GET_CELL (spreadsheet, coordi, coordj);

  /* Création d'une fenêtre popup avec un GtkEntry intégré */
  priv->winentry = gtk_spread_sheet_entry_new (spreadsheet, cell);
  GdkWindow *mainwindow = gtk_widget_get_root_window (GTK_WIDGET (spreadsheet));

  /* Récupération de la position et de la taille graphique de la cellule */
  gtk_spread_sheet_cell_get_graphic_position (cell, &posx, &posy);
  gtk_spread_sheet_cell_get_graphic_size (cell, &width, &height);

  /* Mise à l'échelle de la fenêtre avec le GtkEntry à la taille de la cellule */
  gtk_widget_set_size_request (priv->winentry, width, height);
  gtk_widget_show_all (priv->winentry);
  gdk_window_set_transient_for (gtk_widget_get_window (priv->winentry), mainwindow);
   
  /* Positionnement de la fenêtre sur la cellule */
  gint X, Y;
  gdk_window_get_origin (gtk_widget_get_window (GTK_WIDGET (spreadsheet)), &X, &Y);
  gdk_window_move (gtk_widget_get_window (priv->winentry), X+posx, Y+posy);

  return TRUE;
}

static gboolean
gtk_spread_sheet_callback_double_left_clicked_event (GtkWidget *spreadsheet, GdkEventButton *ev, gpointer user_data)
{
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (GTK_SPREAD_SHEET (spreadsheet));
  gint coordi=-1, coordj=-1;
  GtkSpreadSheetCell *cell = NULL;
  gint posx, posy, width, height;
  GtkAllocation allocation;

  /* Si ce n'est pas un double-clic avec le bouton gauche on sort */
  if (ev->type!=GDK_2BUTTON_PRESS)
    return FALSE;

  /* Si ce n'est pas un double clic avec le bouton gauche on sort */
  if (ev->button!=1)
    return FALSE;

  return gtk_spread_sheet_edit_cell (GTK_SPREAD_SHEET (spreadsheet), ev->x, ev->y);
}

static gboolean
gtk_spread_sheet_callback_left_clicked_event (GtkWidget *spreadsheet, GdkEventButton *ev, gpointer user_data)
{
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (GTK_SPREAD_SHEET (spreadsheet));
  gint coordi=-1, coordj=-1;
  GtkSpreadSheetCell *cell = NULL;
  gint posx, posy, width, height;
  GtkAllocation allocation;

  /* Si c'est un double clic avec le bouton gauche on sort */
  if (ev->type!=GDK_BUTTON_PRESS && ev->type==GDK_2BUTTON_PRESS)
    return FALSE;

  /* Si ce n'est pas un clic avec le bouton gauche on sort */
  if (ev->button!=1)
    return FALSE;

  /* Vérification si une cellule n'est pas en cours d'édition. Si c'est le cas
   * suppression de celle-ci. */
  if (priv->winentry)
    {
      gtk_widget_destroy (priv->winentry);
      priv->winentry = NULL;
    }

  /* Récupération de la cellule actuellement pointée par la souris.
   * Si la souris n'est sur aucune cellule coordi et coordj = -1. */
  gtk_spread_sheet_cell_under_mouse (GTK_SPREAD_SHEET (spreadsheet),
				     ev->x, ev->y, &coordi, &coordj);

  /* Clic en dehors du tableur. Suppression de la sélection */
  if (coordi==-1)
    {
      priv->selecttopleftcell = NULL;
      priv->selectbottomrightcell = NULL;
      return FALSE;
    }

  /* clic sur la cellule [0,0)=> sélection de tout le tableur */
  if (coordi==0 && coordj==0)
    {
      priv->selecttopleftcell = GET_CELL (GTK_SPREAD_SHEET (spreadsheet), 1, 1);
      priv->selectbottomrightcell = GET_CELL (GTK_SPREAD_SHEET (spreadsheet), priv->nb_cols-1, priv->nb_rows-1);

      /* Emission du signal "select_range" pour indiquer qu'une sélection vient d'être effectuée */
      priv->emitted_signal = GTK_SPREAD_SHEET_SELECT_RANGE_SIGNAL;

      return FALSE;
    }
  
  /* Si un clic sur un label d'une ligne mais pas sur son redimensionnement */
  if (coordi==0 && priv->TYPE_REDIM==NONE)
    {
      priv->selecttopleftcell = GET_CELL (GTK_SPREAD_SHEET (spreadsheet), 1, coordj);
      priv->selectbottomrightcell = GET_CELL (GTK_SPREAD_SHEET (spreadsheet), priv->nb_cols-1, coordj);

      /* Emission du signal "select_row" pour indiquer qu'une sélection vient d'être effectuée */
      priv->emitted_signal = GTK_SPREAD_SHEET_SELECT_ROW_SIGNAL;

      return FALSE;
    }

  /* Si un clic sur un label d'une colonne mais pas sur son redimensionnement */
  if (coordj==0 && priv->TYPE_REDIM==NONE)
    {
      priv->selecttopleftcell = GET_CELL (GTK_SPREAD_SHEET (spreadsheet), coordi, 1);
      priv->selectbottomrightcell = GET_CELL (GTK_SPREAD_SHEET (spreadsheet), coordi, priv->nb_rows-1);
 
      /* Emission du signal "select_column" pour indiquer qu'une sélection vient d'être effectuée */
      priv->emitted_signal = GTK_SPREAD_SHEET_SELECT_COLUMN_SIGNAL;

      return FALSE;
    }

  /* Il ne reste plus qu'un clic sur une cellule autre */
  /* Emission du signal "select_range" pour indiquer qu'une sélection vient d'être effectuée */
  priv->selecttopleftcell = GET_CELL (GTK_SPREAD_SHEET (spreadsheet), coordi, coordj);
  priv->selectbottomrightcell = GET_CELL (GTK_SPREAD_SHEET (spreadsheet), coordi, coordj);
  priv->emitted_signal = GTK_SPREAD_SHEET_SELECT_RANGE_SIGNAL;

  return FALSE;
}

static gboolean
gtk_spread_sheet_callback_press_event (GtkWidget *spreadsheet, GdkEventButton *ev, gpointer user_data)
{
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (GTK_SPREAD_SHEET (spreadsheet));
  gint coordi=-1, coordj=-1;
  GtkSpreadSheetCell *cell = NULL;
  gint posx, posy;
  gint width, height;

  /* Vérification : le bouton gauche est enfoncé */
  if (ev->button!=1)
    return FALSE;

  /* Récupération de la cellule actuellement pointée par la souris.
   * Si la souris n'est sur aucune cellule coordi et coordj = -1. */
  gtk_spread_sheet_cell_under_mouse (GTK_SPREAD_SHEET (spreadsheet),
				     ev->x, ev->y, &coordi, &coordj);

  if (coordi!=-1)
    {
      /* Test si la souris est sur le bord gauche (redimensionnement horizontal)
       * ou le bord inférieur (redimensionnement vertical) de la première cellule.
       * Si tel est le cas on fixe la variable TYPE_REDIM en fonction et on active
       * la variable COL_ROW_RESIZE.
       */
      cell = GET_CELL (GTK_SPREAD_SHEET(spreadsheet), coordi, coordj);
      gtk_spread_sheet_cell_get_graphic_position (cell, &posx, &posy);
      gtk_spread_sheet_cell_get_graphic_size (cell, &width, &height);
 
      if (priv->activecell.y==0 && ev->x>=posx + width-2 && priv->activecell.y<=0 && ev->x<=posx + width)
	{
	  /* La souris est sur le bord droit d'une cellule sur la première ligne.
	   * On affiche une ligne verticale verte pour indiquer le redimensionnement
	   * de la colonne. */
	  priv->TYPE_REDIM = COLUMNS;
	  
	  gtk_spread_sheet_rows_cols_color (GTK_SPREAD_SHEET (spreadsheet), coordi, coordj);

	  priv->COL_ROW_RESIZE = TRUE;

	  return FALSE;
	}

      if (priv->activecell.x==0 && ev->y>=posy + height-2 && ev->y<=posy + height)
	{
	  /* La souris est sur le bord inférieur d'une cellule sur la première ligne.
	   * On affiche une ligne horizontale verte pour indiquer le redimensionnement
	   * de la colonne. */
	  priv->TYPE_REDIM = ROWS;
	  
	  gtk_spread_sheet_rows_cols_color (GTK_SPREAD_SHEET (spreadsheet), coordi, coordj);

	  priv->COL_ROW_RESIZE = TRUE;

	  return FALSE;
	}

      /* La souris se trouve sur une cellule quelconque et n'est pas sur le bord
       * droit ou inférieur d'une cellule label.
       * L'utilisateur cherche peut être à effectuer un début de sélection multiple.
       * On active MULTIPLE_SELECT et on sauve la cellule actuellement pointée comme cellule
       * de départ.
       */
      priv->TYPE_REDIM = NONE;
      priv->MULTIPLE_SELECT = TRUE;
 
      gtk_widget_queue_draw (GTK_WIDGET (spreadsheet));
    }
  
  return FALSE;
}

static void
contextmenu_callback (GtkMenuItem *menuitem, GtkSpreadSheet *spreadsheet)
{
  GtkSpreadSheetCell *topleft, *bottomright;
  GdkPoint first, last;

  gtk_spread_sheet_get_selected (spreadsheet, &first, &last);
  topleft = gtk_spread_sheet_get_cell (spreadsheet, first.x, first.y);
  bottomright = gtk_spread_sheet_get_cell (spreadsheet, last.x, last.y);

  if (strstr (gtk_menu_item_get_label (menuitem), "Insert column before"))
    gtk_spread_sheet_insert_col_row (GTK_SPREAD_SHEET (spreadsheet), first.x, COLUMNS, FALSE);
  else if (strstr (gtk_menu_item_get_label (menuitem), "Insert row before"))
    gtk_spread_sheet_insert_col_row (GTK_SPREAD_SHEET (spreadsheet), first.y, ROWS, FALSE);
  else if (strstr (gtk_menu_item_get_label (menuitem), "Insert column after"))
    gtk_spread_sheet_insert_col_row (GTK_SPREAD_SHEET (spreadsheet), first.x, COLUMNS, TRUE);
  else if (strstr (gtk_menu_item_get_label (menuitem), "Insert row after"))
    gtk_spread_sheet_insert_col_row (GTK_SPREAD_SHEET (spreadsheet), first.y, ROWS, TRUE);
  else if (strstr (gtk_menu_item_get_label (menuitem), "Remove column"))
    gtk_spread_sheet_remove_col_row (GTK_SPREAD_SHEET (spreadsheet), first.x, COLUMNS);
  else if (strstr (gtk_menu_item_get_label (menuitem), "Remove row"))
    gtk_spread_sheet_remove_col_row (GTK_SPREAD_SHEET (spreadsheet), first.y, ROWS);
  else if (strstr (gtk_menu_item_get_label (menuitem), "Cut"))
    gtk_spread_sheet_clipboard_set_selection
      (gtk_spread_sheet_get_clipboard (spreadsheet),
       topleft, bottomright,
       CLIP_CUT);
  else if (strstr (gtk_menu_item_get_label (menuitem), "Copy"))
    gtk_spread_sheet_clipboard_set_selection
      (gtk_spread_sheet_get_clipboard (spreadsheet),
       topleft, bottomright,
       CLIP_COPY);
  else if (strstr (gtk_menu_item_get_label (menuitem), "Paste"))
    {
      GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);
      /* Vérification si les cellules destinations contiennent déjà des données */
      gint i, j;
      GtkSpreadSheetCell *cell = NULL, *destination = NULL;
      GdkPoint finalpos;
      gint cols, rows;

      /* Si l'utilisateur cherche à copier plusieurs colonnes/lignes il faut décaler
       * de + 1 la zone destination sinon le label des lignes/colonnes sera écrasé */
      gtk_spread_sheet_cell_get_spreadsheet_position (priv->cellunderpointer, &finalpos);
      if (finalpos.x==0) finalpos.x = 1;
      if (finalpos.y==0) finalpos.y = 1;

      destination = gtk_spread_sheet_get_cell (spreadsheet, finalpos.x, finalpos.y);
      gtk_spread_sheet_cell_get_spreadsheet_position (topleft, &finalpos);
      gtk_spread_sheet_get_size (spreadsheet, &cols, &rows);

      /* Calcul de la largeur et de la hauteur de la sélection */
      gint width, height;
      width = last.x-first.x + 1;
      height = last.y - first.y + 1;

      for (j=finalpos.y; j < finalpos.y + height; j++)
	{
	  for (i=finalpos.x; i < finalpos.x + width; i++)
	    {
	      cell = gtk_spread_sheet_get_cell (spreadsheet, i, j);
	      if (g_strcmp0 (gtk_spread_sheet_cell_get_text (cell), "")!=0)
		{
		  /* La destination n'est pas vide. On demande à l'utilisateur
		   * s'il désire écraser les données.*/
		  GtkWidget *dialog = NULL;

		  dialog = gtk_message_dialog_new (GTK_WINDOW (gtk_spread_sheet_get_toplevel_window (spreadsheet)),
						   GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
						   GTK_MESSAGE_WARNING,
						   GTK_BUTTONS_YES_NO,
						   "The destination cells already contain data. Do you want to overwrite them ?");
		  gtk_window_set_title (GTK_WINDOW (dialog), "SpreadSheet dialog");

		  gint result = gtk_dialog_run (GTK_DIALOG (dialog));
		  switch (result)
		    {
		    case GTK_RESPONSE_YES:
		      gtk_spread_sheet_clipboard_selection_paste (spreadsheet, destination, TRUE);
		      break;
		    }

		  gtk_widget_destroy (dialog);

		  return;
		}
	    }
	}

      gtk_spread_sheet_clipboard_selection_paste (spreadsheet, destination, FALSE);
    }
}
  
static gboolean
gtk_spread_sheet_callback_right_clicked_event (GtkWidget *spreadsheet, GdkEventButton *ev, gpointer user_data)
{
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (GTK_SPREAD_SHEET (spreadsheet));
  gint coordi=-1, coordj=-1;
  GtkSpreadSheetCell *cell = NULL;
  gint posx, posy, width, height;
  GtkAllocation allocation;

  /* Si c'est un double clic on sort */
  if (ev->type!=GDK_BUTTON_PRESS && ev->type==GDK_2BUTTON_PRESS)
    return FALSE;

  /* Si ce n'est pas un clic avec le bouton droit on sort */
  if (ev->button!=3)
    return FALSE;

  /* Vérification si une cellule n'est pas en cours d'édition. Si c'est le cas
   * suppression de celle-ci. */
  if (priv->winentry)
    {
      gtk_widget_destroy (priv->winentry);
      priv->winentry = NULL;
    }

  /* On verifie si la souris est dans la zone sélectionnée lors du clic. Dans le cas contraire on
   * sélectionne la case, la colonne ou la ligne, pointée.*/
  GdkPoint topleft, bottomright, mousepos;
  gboolean selection = FALSE;
  
  gtk_spread_sheet_cell_get_spreadsheet_position (priv->cellunderpointer, &mousepos);
  if (gtk_spread_sheet_get_selected (GTK_SPREAD_SHEET (spreadsheet), &topleft, &bottomright))
    {
      if (gtk_spread_sheet_point_in_rectangle (topleft, bottomright, mousepos.x, mousepos.y))
	selection = TRUE;
    }

  if (!selection)
    {
      /* Si la cellule (0, 0) est sélectionnée on sélectionne toute la table */
      if (mousepos.x==0 && mousepos.y==0)
	{
	  mousepos.x = 1; mousepos.y = 1;
	  bottomright.x = priv->nb_cols-1;
	  bottomright.y = priv->nb_rows-1;
	  gtk_spread_sheet_set_selected (GTK_SPREAD_SHEET (spreadsheet), mousepos, bottomright);
	}
      /* Si la souris est sur un label (ligne ou colonne) il faut sélectionner
       * toute la ligne ou toute la colonne */
      else if (mousepos.x==0 || mousepos.y==0)
	{
	  gint cols, rows;
	  gtk_spread_sheet_get_size (GTK_SPREAD_SHEET (spreadsheet), &cols, &rows);
	  
	  if (mousepos.x==0)
	    {
	      mousepos.x = 1;
	      bottomright.x = mousepos.x + cols - 2;
	      bottomright.y = mousepos.y;
	    }
	  else
	    {
	      mousepos.y=1;
	      bottomright.x = mousepos.x;
	      bottomright.y = mousepos.y + rows - 2;
	    }
	  gtk_spread_sheet_set_selected (GTK_SPREAD_SHEET (spreadsheet), mousepos, bottomright);
	}
      else
	gtk_spread_sheet_set_selected (GTK_SPREAD_SHEET (spreadsheet), mousepos, mousepos);
    }


  /* Affichage du menu contextuel */
  GtkWidget *separatoritem;
  gint index = 0;
  priv->contextmenu = gtk_menu_new ();
  priv->insertcolitembefore = gtk_menu_item_new_with_label ("Insert column before");
  gtk_menu_attach (GTK_MENU (priv->contextmenu), priv->insertcolitembefore, 0, 1, index, index+1);
  index++;
  priv->insertrowitembefore = gtk_menu_item_new_with_label ("Insert row before");
  gtk_menu_attach (GTK_MENU (priv->contextmenu), priv->insertrowitembefore, 0, 1, index, index+1);
  index++;
  priv->insertcolitemafter = gtk_menu_item_new_with_label ("Insert column after");
  gtk_menu_attach (GTK_MENU (priv->contextmenu), priv->insertcolitemafter, 0, 1, index, index+1);
  index++;
  priv->insertrowitemafter = gtk_menu_item_new_with_label ("Insert row after");
  gtk_menu_attach (GTK_MENU (priv->contextmenu), priv->insertrowitemafter, 0, 1, index, index+1);
  index++;
  priv->removecolitem = gtk_menu_item_new_with_label ("Remove column");
  gtk_menu_attach (GTK_MENU (priv->contextmenu), priv->removecolitem, 0, 1, index, index+1);
  index++;
  priv->removerowitem = gtk_menu_item_new_with_label ("Remove row");
  gtk_menu_attach (GTK_MENU (priv->contextmenu), priv->removerowitem, 0, 1, index, index+1);
  index++;
  separatoritem = gtk_separator_menu_item_new ();
  gtk_menu_attach (GTK_MENU (priv->contextmenu), separatoritem, 0, 1, index, index+1);
  index++;
  priv->cutitem = gtk_menu_item_new_with_label ("Cut (Ctrl + X)");
  gtk_menu_attach (GTK_MENU (priv->contextmenu), priv->cutitem, 0, 1, index, index+1);
  index++;
  priv->copyitem = gtk_menu_item_new_with_label ("Copy (Ctrl + C)");
  gtk_menu_attach (GTK_MENU (priv->contextmenu), priv->copyitem, 0, 1, index, index+1);
  index++;
  priv->pasteitem = gtk_menu_item_new_with_label ("Paste (Ctrl + V)");
  gtk_menu_attach (GTK_MENU (priv->contextmenu), priv->pasteitem, 0, 1, index, index+1);

  gtk_menu_popup (GTK_MENU (priv->contextmenu), NULL, NULL, NULL, NULL, 0,
		  gtk_get_current_event_time ());
  gtk_menu_attach_to_widget (GTK_MENU (priv->contextmenu), GTK_WIDGET (spreadsheet), NULL);

  /* Désacivation des entrées d'insertion/suppression de colonnes/lignes si la souris
   * n'est pas sur un label */
  gtk_spread_sheet_cell_get_spreadsheet_position (priv->cellunderpointer, &topleft);
  if (topleft.x==0 && topleft.y!=0)
    {
      gtk_widget_set_sensitive (priv->insertcolitembefore, FALSE);
      gtk_widget_set_sensitive (priv->insertcolitemafter, FALSE);
      gtk_widget_set_sensitive (priv->removecolitem, FALSE);
      if (priv->nb_rows<3)
	gtk_widget_set_sensitive (priv->removerowitem, FALSE);
    }
  if (topleft.x!=0 && topleft.y==0)
    {
      gtk_widget_set_sensitive (priv->insertrowitembefore, FALSE);
      gtk_widget_set_sensitive (priv->insertrowitemafter, FALSE);
      gtk_widget_set_sensitive (priv->removerowitem, FALSE);
      if (priv->nb_cols<3)
	gtk_widget_set_sensitive (priv->removecolitem, FALSE);
    }
  if ((topleft.x==0 && topleft.y==0) || (topleft.x!=0 && topleft.y!=0))
    {
      gtk_widget_set_sensitive (priv->insertcolitembefore, FALSE);
      gtk_widget_set_sensitive (priv->insertcolitemafter, FALSE);
      gtk_widget_set_sensitive (priv->removecolitem, FALSE);
      gtk_widget_set_sensitive (priv->insertrowitembefore, FALSE);
      gtk_widget_set_sensitive (priv->insertrowitemafter, FALSE);
      gtk_widget_set_sensitive (priv->removerowitem, FALSE);
    }

  /* Attachement des callbacks */
  g_signal_connect (G_OBJECT (priv->insertcolitembefore), "activate",
		    G_CALLBACK (contextmenu_callback),
		    GTK_SPREAD_SHEET (spreadsheet));
  g_signal_connect (G_OBJECT (priv->insertrowitembefore), "activate",
		    G_CALLBACK (contextmenu_callback),
		    GTK_SPREAD_SHEET (spreadsheet));
  g_signal_connect (G_OBJECT (priv->insertcolitemafter), "activate",
		    G_CALLBACK (contextmenu_callback),
		    GTK_SPREAD_SHEET (spreadsheet));
  g_signal_connect (G_OBJECT (priv->insertrowitemafter), "activate",
		    G_CALLBACK (contextmenu_callback),
		    GTK_SPREAD_SHEET (spreadsheet));
  g_signal_connect (G_OBJECT (priv->removecolitem), "activate",
		    G_CALLBACK (contextmenu_callback),
		    GTK_SPREAD_SHEET (spreadsheet));
  g_signal_connect (G_OBJECT (priv->removerowitem), "activate",
		    G_CALLBACK (contextmenu_callback),
		    GTK_SPREAD_SHEET (spreadsheet));
  g_signal_connect (G_OBJECT (priv->cutitem), "activate",
		    G_CALLBACK (contextmenu_callback),
		    GTK_SPREAD_SHEET (spreadsheet));
  g_signal_connect (G_OBJECT (priv->copyitem), "activate",
		    G_CALLBACK (contextmenu_callback),
		    GTK_SPREAD_SHEET (spreadsheet));
  g_signal_connect (G_OBJECT (priv->pasteitem), "activate",
		    G_CALLBACK (contextmenu_callback),
		    GTK_SPREAD_SHEET (spreadsheet));

  /* Désactivation des entrées en fonction de l'état du clipboard et des cellules sélectionnées */
  if (gtk_spread_sheet_clipboard_get_function (gtk_spread_sheet_get_clipboard (GTK_SPREAD_SHEET (spreadsheet)))==CLIP_NONE)
    gtk_widget_set_sensitive (priv->pasteitem, FALSE);
  if (!gtk_spread_sheet_get_selected (GTK_SPREAD_SHEET (spreadsheet), &topleft, &bottomright))
    {
      gtk_widget_set_sensitive (priv->cutitem, FALSE);
      gtk_widget_set_sensitive (priv->copyitem, FALSE);
    }

  gtk_widget_show_all (priv->contextmenu);

  return FALSE;
}

static gboolean
gtk_spread_sheet_callback_release_event (GtkWidget *spreadsheet, GdkEventButton *ev, gpointer user_data)
{
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (GTK_SPREAD_SHEET (spreadsheet));

  /* Il y a une sélection multiple en cours. */
  if (priv->MULTIPLE_SELECT)
    {
      gint coordi=-1, coordj=-1;

      /* Récupération de la cellule actuellement pointée par la souris.
       * Si la souris n'est sur aucune cellule coordi et coordj = -1. */
      gtk_spread_sheet_cell_under_mouse (GTK_SPREAD_SHEET (spreadsheet),
					 ev->x, ev->y, &coordi, &coordj);

      /* Mise en ordre des coordonnées de la sélection multiple */
      gtk_spread_sheet_reorder_multiple_selected (GTK_SPREAD_SHEET (spreadsheet));

      if (coordi!=-1)
	{
	  GdkPoint topleft;
	  gtk_spread_sheet_cell_get_spreadsheet_position (priv->selecttopleftcell, &topleft);

	  switch (priv->emitted_signal)
	    {
	    case GTK_SPREAD_SHEET_SELECT_RANGE_SIGNAL:
	      {
		g_signal_emit_by_name (G_OBJECT (spreadsheet), "select_range",
				       priv->selecttopleftcell, priv->selectbottomrightcell);
		break;
	      }
	    case GTK_SPREAD_SHEET_SELECT_COLUMN_SIGNAL:
	      {
		g_signal_emit_by_name (G_OBJECT (spreadsheet), "select_column",
				       priv->selecttopleftcell, priv->selectbottomrightcell);
		break;
	      }
	    case GTK_SPREAD_SHEET_SELECT_ROW_SIGNAL:
	      {
		g_signal_emit_by_name (G_OBJECT (spreadsheet), "select_row",
				       priv->selecttopleftcell, priv->selectbottomrightcell);
		break;
	      }
	    }
	}
      else
	{ /* Le bouton est relâché en dehors du tableur. La sélection
	   * multiple en cours est annulée. */
	  priv->selecttopleftcell = NULL;
	  priv->selectbottomrightcell = NULL;
	}

      priv->MULTIPLE_SELECT = FALSE;
    }
  /* Mise à jour de l'affichage */
  gtk_widget_queue_draw (spreadsheet);

  priv->COL_ROW_RESIZE = FALSE;

  return FALSE;
}

static gboolean
gtk_spread_sheet_callback_scroll_event (GtkWidget *spreadsheet, GdkEvent *event, gpointer user_data)
{
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (GTK_SPREAD_SHEET (spreadsheet));
 
  /* Vérification si une cellule n'est pas en cours d'édition. Si c'est le cas
   * suppression de celle-ci. */
  if (priv->winentry)
    {
      gtk_widget_destroy (priv->winentry);
      priv->winentry = NULL;
    }
  
  return FALSE;
}

static gboolean
gtk_spread_sheet_callback_key_press_event (GtkWidget *spreadsheet, GdkEventKey *ev, gpointer user_data)
{
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (GTK_SPREAD_SHEET (spreadsheet));

  switch (ev->keyval)
    {
    case GDK_KEY_c : // touche "c" enfoncée
      {
	if (ev->state==20) // touche "ctrl" enfoncée en même temps
	  {
	    /* Si aucune case n'est sélectionnée on sort */
	    GdkPoint topleftpos, bottomrightpos;
	    if (!gtk_spread_sheet_get_selected (GTK_SPREAD_SHEET (spreadsheet), &topleftpos, &bottomrightpos))
	      return FALSE;

	    GtkSpreadSheetCell *topleft, *bottomright;
	    GdkPoint first, last;
	    
	    gtk_spread_sheet_get_selected (GTK_SPREAD_SHEET (spreadsheet), &first, &last);
	    topleft = gtk_spread_sheet_get_cell (GTK_SPREAD_SHEET (spreadsheet), first.x, first.y);
	    bottomright = gtk_spread_sheet_get_cell (GTK_SPREAD_SHEET (spreadsheet), last.x, last.y);

	    gtk_spread_sheet_clipboard_set_selection
	      (gtk_spread_sheet_get_clipboard (GTK_SPREAD_SHEET (spreadsheet)),
	       topleft, bottomright, CLIP_COPY);
	  }
	break;
      }
    case GDK_KEY_x : // touche "x" enfoncée
      {
	if (ev->state==20) // touche "ctrl" enfoncée en même temps
	  {
	    /* Si auncune case n'est sélectionnée on sort */
	    GdkPoint topleftpos, bottomrightpos;
	    if (!gtk_spread_sheet_get_selected (GTK_SPREAD_SHEET (spreadsheet), &topleftpos, &bottomrightpos))
	      return FALSE;

	    GtkSpreadSheetCell *topleft, *bottomright;
	    GdkPoint first, last;
	    
	    gtk_spread_sheet_get_selected (GTK_SPREAD_SHEET (spreadsheet), &first, &last);
	    topleft = gtk_spread_sheet_get_cell (GTK_SPREAD_SHEET (spreadsheet), first.x, first.y);
	    bottomright = gtk_spread_sheet_get_cell (GTK_SPREAD_SHEET (spreadsheet), last.x, last.y);

	    gtk_spread_sheet_clipboard_set_selection
	      (gtk_spread_sheet_get_clipboard (GTK_SPREAD_SHEET (spreadsheet)),
	       topleft, bottomright, CLIP_CUT);
	  }
	break;
      }
    case GDK_KEY_v : // touche "v" enfoncée
      {
	if (ev->state==20) // touche "ctrl" enfoncée en même temps
	  {
	    if (gtk_spread_sheet_clipboard_get_function (gtk_spread_sheet_get_clipboard (GTK_SPREAD_SHEET (spreadsheet)))==CLIP_NONE)
	      return FALSE;
	    
	    GtkWidget *menuitem = gtk_menu_item_new_with_label ("Paste (Ctrl + V)");

	    contextmenu_callback (GTK_MENU_ITEM (menuitem), GTK_SPREAD_SHEET (spreadsheet));
	  }
	break;
      }
    case GDK_KEY_Return :
      {
 	/* Vérification si une seule case est sélectionnée */
	if (priv->selecttopleftcell)
	  {
	    GdkPoint topleft, bottomright;
	    gtk_spread_sheet_cell_get_spreadsheet_position (priv->selecttopleftcell, &topleft);
	    gtk_spread_sheet_cell_get_spreadsheet_position (priv->selectbottomrightcell, &bottomright);
	    
	    if (topleft.x == bottomright.x && topleft.y == bottomright.y)
	      {
		GtkSpreadSheetCell *cell = NULL;
		gint x, y;
		cell = gtk_spread_sheet_get_cell (GTK_SPREAD_SHEET (spreadsheet),
						  topleft.x,
						  topleft.y);
		gtk_spread_sheet_cell_get_graphic_position (cell, &x, &y);

		/* Appel du callback normalement activé lors d'un double clic */
		gtk_spread_sheet_edit_cell (GTK_SPREAD_SHEET (spreadsheet), x+2, y+2);

		break;
	      }
	  }
  	break;
      }
    case GDK_KEY_Left :
      {
	if (priv->selecttopleftcell)
	  {
	    GdkPoint first, last;
	    gtk_spread_sheet_get_selected (GTK_SPREAD_SHEET (spreadsheet), &first, &last);
	    if (first.x > 1)
	      {
		first.x--; last.x--;
	      }

	    gtk_spread_sheet_set_selected (GTK_SPREAD_SHEET (spreadsheet), first, last);

	    /* Emission du signal "select_move" pour indiquer qu'une sélection vient de bouger */
	    g_signal_emit_by_name (G_OBJECT (spreadsheet), "select_move",
				   GET_CELL (GTK_SPREAD_SHEET (spreadsheet), first.x, first.y),
				   GET_CELL (GTK_SPREAD_SHEET (spreadsheet), last.x, last.y));
	  }
  	break;
      }
    case GDK_KEY_Right :
      {
	if (priv->selecttopleftcell)
	  {
	    GdkPoint first, last;
	    gtk_spread_sheet_get_selected (GTK_SPREAD_SHEET (spreadsheet), &first, &last);
	    if (last.x < priv->nb_cols-1)
	      {
		first.x++; last.x++;
	      }

	    gtk_spread_sheet_set_selected (GTK_SPREAD_SHEET (spreadsheet), first, last);

	    /* Emission du signal "select_move" pour indiquer qu'une sélection vient de bouger */
	    g_signal_emit_by_name (G_OBJECT (spreadsheet), "select_move",
				   GET_CELL (GTK_SPREAD_SHEET (spreadsheet), first.x, first.y),
				   GET_CELL (GTK_SPREAD_SHEET (spreadsheet), last.x, last.y));
	  }
   	break;
      }
    case GDK_KEY_Up :
      {
	if (priv->selecttopleftcell)
	  {
	    GdkPoint first, last;
	    gtk_spread_sheet_get_selected (GTK_SPREAD_SHEET (spreadsheet), &first, &last);
	    if (first.y > 1)
	      {
		first.y--; last.y--;
	      }

	    gtk_spread_sheet_set_selected (GTK_SPREAD_SHEET (spreadsheet), first, last);

	    /* Emission du signal "select_move" pour indiquer qu'une sélection vient de bouger */
	    g_signal_emit_by_name (G_OBJECT (spreadsheet), "select_move",
				   GET_CELL (GTK_SPREAD_SHEET (spreadsheet), first.x, first.y),
				   GET_CELL (GTK_SPREAD_SHEET (spreadsheet), last.x, last.y));
	  }
  	break;
      }
    case GDK_KEY_Down :
      {
	if (priv->selecttopleftcell)
	  {
	    GdkPoint first, last;
	    gtk_spread_sheet_get_selected (GTK_SPREAD_SHEET (spreadsheet), &first, &last);
	    if (last.y < priv->nb_rows-1)
	      {
		first.y++; last.y++;
	      }

	    gtk_spread_sheet_set_selected (GTK_SPREAD_SHEET (spreadsheet), first, last);

	    /* Emission du signal "select_move" pour indiquer qu'une sélection vient de bouger */
	    g_signal_emit_by_name (G_OBJECT (spreadsheet), "select_move",
				   GET_CELL (GTK_SPREAD_SHEET (spreadsheet), first.x, first.y),
				   GET_CELL (GTK_SPREAD_SHEET (spreadsheet), last.x, last.y));
	  }
  	break;
      }
    }

  gtk_widget_queue_draw (spreadsheet);

  return FALSE;
}

static void
gtk_spread_sheet_callback_changed_event (GtkSpreadSheet *spreadsheet,
					 GtkSpreadSheetCell *cell, gpointer user_data)
{
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  if (priv->winentry)
    {
      gtk_widget_destroy (priv->winentry);
      priv->winentry = NULL;
    }
}

static void
gtk_spread_sheet_callback_realize (GtkWidget *widget, gpointer user_data)
{
  GtkWidget *toplevel = gtk_widget_get_toplevel (widget);
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (GTK_SPREAD_SHEET (widget));
  
  if (gtk_widget_is_toplevel (toplevel))
    priv->rootwindow = toplevel;
}

GtkWidget*
gtk_spread_sheet_new (gint width, gint height)
{
  GtkSpreadSheet *spreadsheet = NULL;

  g_return_val_if_fail (width>0, NULL);
  g_return_val_if_fail (height>0, NULL);

  spreadsheet = g_object_new (GTK_TYPE_SPREAD_SHEET, NULL);
  g_return_val_if_fail (spreadsheet!=NULL, NULL);

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  /* Initialisation du tableur */
  gtk_spread_sheet_tab_init (spreadsheet, width+1, height+1);

  /* Initialisation des flêches de la souris */
  priv->colcursor = gdk_cursor_new (GDK_SB_H_DOUBLE_ARROW);
  priv->rowcursor = gdk_cursor_new (GDK_SB_V_DOUBLE_ARROW);

  /* Initialisation du clipboard interne */
  priv->clipboard = gtk_spread_sheet_clipboard_new ();

  /* Ajouts des écouteurs */
  gtk_widget_set_events (GTK_WIDGET (spreadsheet), GDK_POINTER_MOTION_MASK |
  			 GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK |
  			 GDK_KEY_PRESS_MASK | GDK_KEY_RELEASE_MASK | GDK_STRUCTURE_MASK);
 
  /* Connexion des différents signaux */
  g_signal_connect (G_OBJECT (spreadsheet), "realize",
		    G_CALLBACK (gtk_spread_sheet_callback_realize), NULL);
  g_signal_connect (G_OBJECT (spreadsheet), "expose-event" ,
  		    G_CALLBACK (gtk_spread_sheet_callback_expose_tab), NULL);
  g_signal_connect (G_OBJECT (spreadsheet), "motion-notify-event",
  		    G_CALLBACK (gtk_spread_sheet_callback_motion), NULL);
  g_signal_connect (G_OBJECT (spreadsheet), "button-press-event",
  		    G_CALLBACK (gtk_spread_sheet_callback_press_event), NULL);
  g_signal_connect (G_OBJECT (spreadsheet), "button-press-event",
  		    G_CALLBACK (gtk_spread_sheet_callback_left_clicked_event), NULL);
  g_signal_connect (G_OBJECT (spreadsheet), "button-press-event",
  		    G_CALLBACK (gtk_spread_sheet_callback_right_clicked_event), NULL);
  g_signal_connect (G_OBJECT (spreadsheet), "button-press-event",
  		    G_CALLBACK (gtk_spread_sheet_callback_double_left_clicked_event), NULL);
  g_signal_connect (G_OBJECT (spreadsheet), "button-release-event",
  		    G_CALLBACK (gtk_spread_sheet_callback_release_event), NULL);
  g_signal_connect (G_OBJECT (spreadsheet), "scroll-event",
		    G_CALLBACK (gtk_spread_sheet_callback_scroll_event), NULL);

  /* Gestion clavier */
  gtk_widget_set_can_focus (GTK_WIDGET (spreadsheet), TRUE);
  g_signal_connect (G_OBJECT (spreadsheet), "key-press-event",
		    G_CALLBACK (gtk_spread_sheet_callback_key_press_event), NULL);
  g_signal_connect (G_OBJECT (spreadsheet), "changed",
		    G_CALLBACK (gtk_spread_sheet_callback_changed_event), NULL);

  return GTK_WIDGET (spreadsheet);
}

GtkSpreadSheetCell*
gtk_spread_sheet_get_cell (GtkSpreadSheet *spreadsheet, gint i, gint j)
{
  g_return_val_if_fail (spreadsheet!=NULL, NULL);
  g_return_val_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet), NULL);
  if (i<0 || j<0) return NULL;

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);
  g_return_val_if_fail (i<priv->nb_cols, NULL);
  g_return_val_if_fail (j<priv->nb_rows, NULL);
  
  gchar *key = g_strdup_printf("c%dr%d", i, j);
  GtkSpreadSheetCell *cell = g_hash_table_lookup (priv->tab, key);
  g_free (key);

  return cell;
}

void
gtk_spread_sheet_get_size (GtkSpreadSheet *spreadsheet, gint *cols, gint *rows)
{
  if (cols)
    *cols = -1;
  if (rows)
    *rows = -1;
  
  g_return_if_fail (spreadsheet != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  if (cols)
    *cols = priv->nb_cols;
  if (rows)
    *rows = priv->nb_rows;
}

void
gtk_spread_sheet_resize (GtkSpreadSheet *spreadsheet, gint cols, gint rows)
{
  g_return_if_fail (spreadsheet != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));

  if (cols<2) cols=2;
  if (rows<2) rows=2;

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  /* Suppression ou ajout de colonnes */
  if (cols<priv->nb_cols)
    {
      while (priv->nb_cols>cols)
	gtk_spread_sheet_remove_col_row (spreadsheet, priv->nb_cols, COLUMNS);
    }
  else if (cols>priv->nb_cols)
    {
      while (priv->nb_cols<cols)
	gtk_spread_sheet_insert_col_row (spreadsheet, priv->nb_cols, COLUMNS, TRUE);
    }

  /* Suppression ou ajout de lignes */
  if (rows<priv->nb_rows)
    {
      while (priv->nb_rows>rows)
	gtk_spread_sheet_remove_col_row (spreadsheet, priv->nb_rows, ROWS);
    }
  else if (rows>priv->nb_rows)
    {
      while (priv->nb_rows<rows)
	gtk_spread_sheet_insert_col_row (spreadsheet, priv->nb_rows, ROWS, TRUE);
    }
}

void
gtk_spread_sheet_auto_resize_graphic_cell (GtkSpreadSheet *spreadsheet)
{
  g_return_if_fail (spreadsheet != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);
  cairo_t *cr = gdk_cairo_create (gtk_widget_get_window (GTK_WIDGET(spreadsheet)));
  gint i, j;
  GtkSpreadSheetCell *cell = NULL;
  gint width, height;
  cairo_text_extents_t extents;

  /* Taille du texte */
  cairo_set_font_size (cr, 15);

  for (j=0; j<priv->nb_rows; j++)
    {
      for (i=0; i<priv->nb_cols; i++)
	{
	  cell = GET_CELL (GTK_SPREAD_SHEET (spreadsheet), i, j);
	  
	  /* Récupération de la taille graphique du texte de la cellule */
	  cairo_text_extents (cr, gtk_spread_sheet_cell_get_text (cell), &extents);

	  /* Récupération de la taille actuelle de la cellule */
	  gtk_spread_sheet_cell_get_graphic_size (cell, &width, &height);

	  /* Si la taille de la cellule est trop petite pour le texte on
	   * l'agrandit */
	  if ((gint)extents.width>width)
	    {
	      gtk_spread_sheet_cell_set_graphic_size (cell, (gint)extents.width+10, height);
	      width = (gint)extents.width+10;
	    }
	  
	  if ((gint)extents.height>height)
	    {
	      gtk_spread_sheet_cell_set_graphic_size (cell, width, extents.height+10);
	      height = (gint)extents.height+10;
	    }

	  /* Récupération de la nouvelle taille de la cellule */
	  gtk_spread_sheet_cell_get_graphic_size (cell, &width, &height);
	}
    }

  cairo_destroy (cr);
  
  /* Calibrage de toute la table en fonction de la nouvelle taille */
  gint maxwidth=0, maxheight=0;
  for (i=0; i<priv->nb_cols; i++)
    {
      maxheight = 0; maxwidth=0;
      /* Calcul de la largeur max pour la colonne considérée */
      for (j=0; j<priv->nb_rows; j++)
	{
	  cell = GET_CELL (GTK_SPREAD_SHEET (spreadsheet), i, j);
	  gtk_spread_sheet_cell_get_graphic_size (cell, &width, &height);

	  if (width>maxwidth)
	    maxwidth = width;
	  if (height>maxheight)
	    maxheight = height;
	}

      for (j=0; j<priv->nb_rows; j++)
	{
	  /* key = g_strdup_printf ("%d%d", i, j); */
	  cell = GET_CELL (GTK_SPREAD_SHEET (spreadsheet), i, j);
	  /* g_free (key); */
	  gtk_spread_sheet_cell_get_graphic_size (cell, NULL, &height);
	  gtk_spread_sheet_cell_set_graphic_size (cell, maxwidth, height);
	}
    }

  /* Dimensionnement de la hauteur de toutes les cellules */
  for (j=0; j<priv->nb_rows; j++)
    {
      for (i=0; i<priv->nb_cols; i++)
	{
	  cell = GET_CELL (GTK_SPREAD_SHEET (spreadsheet), i, j);
	  gtk_spread_sheet_cell_get_graphic_size (cell, &width, &height);
	  gtk_spread_sheet_cell_set_graphic_size (cell, width, maxheight);
	}
    }
  /* Déplacement des cellules en fct de leur nouvelle taille */
  gtk_spread_sheet_tab_reorder (spreadsheet);

  /* Calcul de la nouvelle taille de l'espace graphique */
  gtk_spread_sheet_screen_size_changed (spreadsheet);
}

GtkSpreadSheetCell
*gtk_spread_sheet_get_last_cell_changed (GtkSpreadSheet *spreadsheet)
{
  g_return_val_if_fail (spreadsheet != NULL, NULL);
  g_return_val_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet), NULL);

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  return priv->lastcellchanged;
}

void
gtk_spread_sheet_set_change_cell (GtkSpreadSheet *spreadsheet, GtkSpreadSheetCell *cell)
{
  g_return_if_fail (spreadsheet != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));
  g_return_if_fail (cell != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET_CELL (cell));

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  priv->lastcellchanged = cell;
}

void
gtk_spread_sheet_set_selected (GtkSpreadSheet *spreadsheet, GdkPoint first, GdkPoint last)
{
  g_return_if_fail (spreadsheet != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  /* On borne les coordonnées transmises */
  if (first.x<0)
    first.x = 0;
  if (first.x > priv->nb_cols)
    first.x = priv->nb_cols-1;

  if (first.y<0)
    first.y = 0;
  if (first.y > priv->nb_rows)
    first.y = priv->nb_rows-1;

  if (last.x<0)
    last.x = 0;
  if (last.x > priv->nb_cols)
    last.x = priv->nb_cols-1;

  if (last.y<0)
    last.y = 0;
  if (last.y > priv->nb_rows)
    last.y = priv->nb_rows-1;

  priv->selecttopleftcell = GET_CELL (spreadsheet, first.x, first.y);
  priv->selectbottomrightcell = GET_CELL (spreadsheet, last.x, last.y);

  /* Remise dans l'ordre des coordonnées de la sélection transmise */
  gtk_spread_sheet_reorder_multiple_selected (GTK_SPREAD_SHEET (spreadsheet));

  gtk_widget_queue_draw (GTK_WIDGET (spreadsheet));
}

gboolean
gtk_spread_sheet_get_selected (GtkSpreadSheet *spreadsheet, GdkPoint *first, GdkPoint *last)
{
  g_return_val_if_fail (spreadsheet != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet), FALSE);
  g_return_val_if_fail (first!=NULL, FALSE);
  g_return_val_if_fail (last!=NULL, FALSE);

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);
 
  if (!priv->selecttopleftcell)
    return FALSE;

  gtk_spread_sheet_cell_get_spreadsheet_position (priv->selecttopleftcell, first);
  gtk_spread_sheet_cell_get_spreadsheet_position (priv->selectbottomrightcell, last);
 
  return TRUE;
}

void
gtk_spread_sheet_deselect (GtkSpreadSheet *spreadsheet)
{
  g_return_if_fail (spreadsheet != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  priv->selecttopleftcell = NULL;
  priv->selectbottomrightcell = NULL;

  gtk_widget_queue_draw (GTK_WIDGET (spreadsheet));
}

void gtk_spread_sheet_set_multiple_selected_colors (GtkSpreadSheet *spreadsheet, ColorCell *bgcolor, ColorCell *textcolor)
{
  g_return_if_fail (spreadsheet != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  /* Il n'y a pas de sélection courante. */
  if (!priv->selecttopleftcell)
    return;
  
  gint i,j;
  GtkSpreadSheetCell *cell = NULL;
  GdkPoint topleft, bottomright;
  gtk_spread_sheet_cell_get_spreadsheet_position (priv->selecttopleftcell, &topleft);
  gtk_spread_sheet_cell_get_spreadsheet_position (priv->selectbottomrightcell, &bottomright);
  for (j=topleft.y; j < bottomright.y; j++)
    {
      for (i=topleft.x; i < bottomright.x; i++)
	{
	  cell = gtk_spread_sheet_get_cell (spreadsheet, i, j);

	  /* Si une couleur de fond est transmise */
	  if (bgcolor)
	    gtk_spread_sheet_cell_set_bgcolor (cell, bgcolor->red, bgcolor->green, bgcolor->blue);

	  /* Si une couleur de texte est transmise */
	  if (textcolor)
	    gtk_spread_sheet_cell_set_textcolor (cell, textcolor->red,textcolor->green, textcolor->blue);
	}
    }

  gtk_widget_queue_draw (GTK_WIDGET (spreadsheet));
}

void
gtk_spread_sheet_set_direction_of_selected_cell (GtkSpreadSheet *spreadsheet, GTK_SPREAD_SHEET_MOVE move)
{
  g_return_if_fail (spreadsheet != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));
  g_return_if_fail (move==GTK_SPREAD_SELECT_MOVE_LEFT ||
		    move==GTK_SPREAD_SELECT_MOVE_RIGHT ||
		    move==GTK_SPREAD_SELECT_MOVE_UP ||
		    move==GTK_SPREAD_SELECT_MOVE_DOWN);

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  priv->selected_enter_move = move;
}

GTK_SPREAD_SHEET_MOVE
gtk_spread_sheet_get_direction_of_selected_cell (GtkSpreadSheet *spreadsheet)
{
  g_return_val_if_fail (spreadsheet != NULL, UNDEFINED);
  g_return_val_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet), UNDEFINED);

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  return priv->selected_enter_move;
}

void
gtk_spread_sheet_set_labels_increment (GtkSpreadSheet *spreadsheet, TYPE_OF_INCREMENT increment)
{
  g_return_if_fail (spreadsheet != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));
  g_return_if_fail (increment==AUTO_INCREMENT || increment==USER_INCREMENT);
  
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  priv->increment = increment;
}

TYPE_OF_INCREMENT
gtk_spread_sheet_get_labels_increment (GtkSpreadSheet *spreadsheet)
{
  g_return_val_if_fail (spreadsheet != NULL, UNDEFINED);
  g_return_val_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet), UNDEFINED);
  
  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  return priv->increment;
}

void
gtk_spread_sheet_set_column_label (GtkSpreadSheet *spreadsheet, gint column, const gchar *label)
{
  g_return_if_fail (spreadsheet != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  if (column<1 || column>=priv->nb_cols)
    return;

  gtk_spread_sheet_cell_set_text (gtk_spread_sheet_get_cell (spreadsheet, column, 0), label);
}

void
gtk_spread_sheet_set_row_label (GtkSpreadSheet *spreadsheet, gint row, const gchar *label)
{
  g_return_if_fail (spreadsheet != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  if (row<1 || row>=priv->nb_rows)
    return;

  gtk_spread_sheet_cell_set_text (gtk_spread_sheet_get_cell (spreadsheet, 0, row), label);
}

void
gtk_spread_sheet_set_origin_column_number (GtkSpreadSheet *spreadsheet, gint origin)
{
  g_return_if_fail (spreadsheet != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  gint i=1, index;
  gchar *text = NULL;

  for (index=origin; index<origin+priv->nb_cols; index++, i++)
    {
      if (i<priv->nb_cols)
	{
	  text = g_strdup_printf ("%d", index);
	  gtk_spread_sheet_cell_set_text (gtk_spread_sheet_get_cell (spreadsheet, i, 0), text);
	  g_free (text);
	}
    }
}

void
gtk_spread_sheet_set_origin_row_number (GtkSpreadSheet *spreadsheet, gint origin)
{
  g_return_if_fail (spreadsheet != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet));

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  gint j=1, index;
  gchar *text = NULL;

  for (index=origin; index<origin+priv->nb_rows; index++, j++)
    {
      if (j<priv->nb_rows)
	{
	  text = g_strdup_printf ("%d", index);
	  gtk_spread_sheet_cell_set_text (gtk_spread_sheet_get_cell (spreadsheet, 0, j), text);
	  g_free (text);
	}
    }
}

GtkWidget*
gtk_spread_sheet_get_entry (GtkSpreadSheet *spreadsheet)
{
  g_return_val_if_fail (spreadsheet != NULL, UNDEFINED);
  g_return_val_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet), UNDEFINED);

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  return priv->winentry;
}

GtkWidget*
gtk_spread_sheet_get_toplevel_window (GtkSpreadSheet *spreadsheet)
{
  g_return_val_if_fail (spreadsheet != NULL, UNDEFINED);
  g_return_val_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet), UNDEFINED);

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  return priv->rootwindow;
}

GtkSpreadSheetClipBoard*
gtk_spread_sheet_get_clipboard (GtkSpreadSheet *spreadsheet)
{
  g_return_val_if_fail (spreadsheet != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet), FALSE);

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  return priv->clipboard;
}

gboolean
gtk_spread_sheet_clipboard_selection_paste (GtkSpreadSheet *spreadsheet,
					    GtkSpreadSheetCell *destination,
					    gboolean overwrite)
{
  g_return_val_if_fail (spreadsheet != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_SPREAD_SHEET (spreadsheet), FALSE);
  g_return_val_if_fail (destination != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_SPREAD_SHEET_CELL (destination), FALSE);

  GtkSpreadSheetPrivate *priv = gtk_spread_sheet_get_instance_private (spreadsheet);

  /* Vérification si la surface de destination est assez grande pour recevoir la copie. */
  GdkPoint finalpos, topleftpos, bottomrightpos;
  gint cols, rows;
  GtkSpreadSheetCell *topleft, *bottomright;
  gtk_spread_sheet_cell_get_spreadsheet_position (destination, &finalpos);
  gtk_spread_sheet_get_size (spreadsheet, &cols, &rows);
  gtk_spread_sheet_clipboard_get_selection (priv->clipboard, &topleft, &bottomright);
  /* Calcul de la largeur et de la hauteur de la sélection */
  gint width, height;
  gtk_spread_sheet_cell_get_spreadsheet_position (topleft, &topleftpos);
  gtk_spread_sheet_cell_get_spreadsheet_position (bottomright, &bottomrightpos);
  
  width = bottomrightpos.x-topleftpos.x + 1;
  height = bottomrightpos.y - topleftpos.y + 1;

  if (finalpos.x+width > cols || finalpos.y+height > rows)
    {
      /* La destination n'est pas assez grande pour coller le presse papier */
      GtkWidget *dialog = NULL;

      dialog = gtk_message_dialog_new (GTK_WINDOW (gtk_spread_sheet_get_toplevel_window (spreadsheet)),
				       GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
				       GTK_MESSAGE_WARNING,
				       GTK_BUTTONS_CLOSE,
				       "The destination area is not big enough to paste.");
      gtk_window_set_title (GTK_WINDOW (dialog), "SpreadSheet dialog");
      
      gint result = gtk_dialog_run (GTK_DIALOG (dialog));

      gtk_widget_destroy (dialog);

      return FALSE;
    }

  /* Si overwirte vaut FALSE il faut vérifier que la destination est vide */
  if (!overwrite)
    {
      gint i, j;
      GtkSpreadSheetCell *cell = NULL;
      for (j=finalpos.y; j < finalpos.y + height; j++)
	{
	  for (i=finalpos.x; i < finalpos.x + width; i++)
	    {
	      cell = gtk_spread_sheet_get_cell (spreadsheet, i, j);
	      if (g_strcmp0 (gtk_spread_sheet_cell_get_text (cell), "")!=0)
		{
		  g_printerr ("clipboard destination copy is not empty !\n");
		  return FALSE;
		}
	    }
	}
    }

  /* À partir d'ici on peut effectuer le "collage" de la sélection */
  gint i, j;
  GtkSpreadSheetCell *srccell = NULL, *destcell = NULL;

  for (j=finalpos.y; j < finalpos.y + height; j++)
    {
      for (i=finalpos.x; i < finalpos.x + width; i++)
	{
	  srccell = gtk_spread_sheet_get_cell (spreadsheet, topleftpos.x + (i-finalpos.x), topleftpos.y + (j-finalpos.y));
	  destcell = gtk_spread_sheet_get_cell (spreadsheet, i, j);
	  gtk_spread_sheet_cell_set_text (destcell, gtk_spread_sheet_cell_get_text (srccell));

	  /* Si la fonction de départ est CLIP_CUT il faut effacer les cellules sources */
	  if (gtk_spread_sheet_clipboard_get_function (priv->clipboard) == CLIP_CUT)
	    gtk_spread_sheet_cell_set_text (srccell, "");
	}
    }

  /* Initialisation du clipboard */
  gtk_spread_sheet_clipboard_erase_selection (priv->clipboard);

  return TRUE;
}
