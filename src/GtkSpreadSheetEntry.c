#include "GtkSpreadSheetEntry.h"

typedef struct
{
  GtkWidget *window;
  GtkSpreadSheet *spreadsheet;
  GtkSpreadSheetCell *cell;
} SEntry;

static void
gtk_spread_sheet_entry_callback_activate_entry (GtkEntry *entry, SEntry *data)
{
  /* Insertion du texte modifié dans la cellule */
  gtk_spread_sheet_cell_set_text (data->cell, gtk_entry_get_text (entry));

  /* transmission de la cellule modifiée pour l'utilisateur */
  gtk_spread_sheet_set_change_cell (data->spreadsheet, data->cell);

  /* Récupération de la sélection courante */
  GdkPoint first, last;
  gtk_spread_sheet_get_selected (data->spreadsheet, &first, &last);

  /* Sélection de la cellule adjacente en fonction de GTK_SPREAD_SHEET_MOVE */
  switch (gtk_spread_sheet_get_direction_of_selected_cell (data->spreadsheet))
    {
    case GTK_SPREAD_SELECT_MOVE_LEFT:
      {
	first.x--; last.x--;
	if (first.x==0)
	  {
	    first.x=1; last.x=1;
	  }
	
	break;
      }
    case GTK_SPREAD_SELECT_MOVE_RIGHT:
      {
	gint cols;
	gtk_spread_sheet_get_size (data->spreadsheet, &cols, NULL);
	first.x++; last.x++;
	if (first.x>=cols)
	  {
	    first.x=cols-1; last.x=cols-1;
	  }
	gtk_spread_sheet_set_selected (data->spreadsheet, first, last);
	break;
      }
    case GTK_SPREAD_SELECT_MOVE_UP:
      {
	first.y--; last.y--;
	if (first.y==0)
	  {
	    first.y=1; last.y=1;
	  }
	gtk_spread_sheet_set_selected (data->spreadsheet, first, last);
	break;
      }
    case GTK_SPREAD_SELECT_MOVE_DOWN:
      {
	gint rows;
	gtk_spread_sheet_get_size (data->spreadsheet, NULL, &rows);
	first.y++; last.y++;
	if (first.y>=rows)
	  {
	    first.y=rows-1; last.y=rows-1;
	  }
	gtk_spread_sheet_set_selected (data->spreadsheet, first, last);
      }
    }

  /* Mise à jour de la sélection */
  gtk_spread_sheet_set_selected (data->spreadsheet, first, last);
  gtk_widget_queue_draw (GTK_WIDGET (data->spreadsheet));

  /* Emission du signal "changed" pour indiquer qu'une donnée est modifiée dans le tableur */
  g_signal_emit_by_name (G_OBJECT (data->spreadsheet), "changed", data->cell);

  /* Libération mémoire */
  g_free (data);
}

static gboolean
gtk_spread_sheet_entry_callback_root_window_configure (GtkWidget *widget, GdkEventConfigure *ev, GtkSpreadSheet *spreadsheet)
{
  GtkSpreadSheetCell *cell = NULL;
  gint posx, posy;
  GdkPoint topleft, bottomright;

  if (gtk_spread_sheet_get_entry(spreadsheet))
    {
      /* Récupération de la position de la cellule */
      gtk_spread_sheet_get_selected (spreadsheet, &topleft, &bottomright);
      cell = gtk_spread_sheet_get_cell (spreadsheet, topleft.x, topleft.y);
      gtk_spread_sheet_cell_get_graphic_position (cell, &posx, &posy);

      /* Positionnement de la fenêtre sur la cellule */
      gint X, Y;
      gdk_window_get_origin (gtk_widget_get_window (GTK_WIDGET (spreadsheet)), &X, &Y);
      gdk_window_move (gtk_widget_get_window (gtk_spread_sheet_get_entry(spreadsheet)), X+posx, Y+posy);
    }
  return FALSE;
}

GtkWidget*
gtk_spread_sheet_entry_new (GtkSpreadSheet *spreadsheet, GtkSpreadSheetCell *cell)
{
  GtkWidget *entry=NULL;
  SEntry *data = g_new (SEntry, 1);
  const gchar *text = NULL;

  data->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_decorated (GTK_WINDOW (data->window), FALSE);
  gtk_window_set_skip_pager_hint (GTK_WINDOW (data->window), FALSE);

  data->spreadsheet = spreadsheet;
  data->cell = cell;
  
  entry = gtk_entry_new ();
  text = gtk_spread_sheet_cell_get_text (cell);
  if (text)
    gtk_entry_set_text (GTK_ENTRY (entry), text);
  gtk_container_add (GTK_CONTAINER (data->window), entry);

  g_signal_connect (G_OBJECT (gtk_spread_sheet_get_toplevel_window (spreadsheet)), "configure-event",
		    G_CALLBACK (gtk_spread_sheet_entry_callback_root_window_configure), spreadsheet);
  g_signal_connect (G_OBJECT (entry), "activate",
		    G_CALLBACK (gtk_spread_sheet_entry_callback_activate_entry), data);
  g_signal_connect (G_OBJECT (data->window), "destroy",
		    G_CALLBACK (gtk_widget_destroy), NULL);
  
  return data->window;
}
