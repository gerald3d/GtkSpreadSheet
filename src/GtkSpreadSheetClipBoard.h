/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * GtkSpreadSheetClipBoard.h
 * Copyright (C) 2018 Gerald Dumas <gerald.dumas@laposte.net>
 * 
 * GtkSpreadSheet is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * GtkSpreadSheet is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GTK_SPREAD_SHEET_CLIPBOARD_H__
#define __GTK_SPREAD_SHEET_CLIPBOARD_H__

#include <gtk/gtk.h>
#include "GtkSpreadSheetCell.h"

G_BEGIN_DECLS

#define GTK_TYPE_SPREAD_SHEET_CLIPBOARD            (gtk_spread_sheet_clipboard_get_type ())
#define GTK_SPREAD_SHEET_CLIPBOARD(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_SPREAD_SHEET_CLIPBOARD, GtkSpreadSheetClipBoard))
#define GTK_SPREAD_SHEET_CLIPBOARD_CLASS(class)    (G_TYPE_CHECK_CLASS_CAST ((class), GTK_TYPE_SPREAD_SHEET_CLIPBOARD, GtkSpreadSheetClipBoardClass))
#define GTK_IS_SPREAD_SHEET_CLIPBOARD(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_SPREAD_SHEET_CLIPBOARD))
#define GTK_IS_SPREAD_SHEET_CLIPBOARD_CLASS(class) (G_TYPE_CHECK_CLASS_TYPE ((class), GTK_TYPE_SPREAD_SHEET_CLIPBOARD))
#define GTK_SPREAD_SHEET_CLIPBOARD_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GTK_TYPE_SPREAD_SHEET_CLIPBOARD, GtkSpreadSheetClipBoardClass))
typedef struct _GtkSpreadSheetClipBoard GtkSpreadSheetClipBoard;
typedef struct _GtkSpreadSheetClipBoardClass GtkSpreadSheetClipBoardClass;
typedef struct _GtkSpreadSheetClipBoardPrivate GtkSpreadSheetClipBoardPrivate;

GType gtk_spread_sheet_clipboard_get_type ();

/**
 * GTK_SPREAD_SHEET_CLIPBOARD_FUNC:
 * @CLIP_NONE: no selection
 * @CLIP_CUT: cut function
 * @CLIP_COPY: copy function
 *
 */
typedef enum {CLIP_NONE, CLIP_CUT, CLIP_COPY} GTK_SPREAD_SHEET_CLIPBOARD_FUNC;

/**
 * GtkSpreadSheetClipBoard:
 *
 * All the fields in the #GtkSpreadSheetClipBoard structure are private to the
 * #GtkSpreadSheetClipBoard implementation and should never be accessed directly.
 */
struct _GtkSpreadSheetClipBoard {
  /*< private >*/
  GObject parent;

  GtkSpreadSheetClipBoardPrivate *priv;
};

/**
 * GtkSpreadSheetClipBoardPrivate:
 *
 * All the fields in the #GtkSpreadSheetClipBoardPrivate structure are private
 * to the #GtkSpreadSheetClipBoardPrivate implementation and should never be
 * accessed directly.
 */
struct _GtkSpreadSheetClipBoardPrivate
{
  /*< private >*/
  /* Données de gestion de la sélection multiple */
  GtkSpreadSheetCell *selecttopleftcell;
  GtkSpreadSheetCell *selectbottomrightcell;
  GTK_SPREAD_SHEET_CLIPBOARD_FUNC func;
};

/**
 * GtkSpreadSheetClipBoardClass:
 */
struct _GtkSpreadSheetClipBoardClass
{
  /*< private >*/
   GObjectClass parent_class;
};

/**
 * gtk_spread_sheet_clipboard_new:
 *
 * Create a new clipboard. It's possible to cut, copy and paste cells in a #GtkSpreadSheet.
 *
 * Returns: a #GtkSpreadSheetClipBoard.
 *
 */
GtkSpreadSheetClipBoard* gtk_spread_sheet_clipboard_new ();

/**
 * gtk_spread_sheet_clipboard_get_selection:
 * @spreadsheetclipboard: a GtkSpreadSheetClipboard
 * @topleft: selected cell
 * @bottomright: selected cell
 *
 * Returns in @topleft and @bottomright the cells which define an actual selection or #NULL
 * if there is no actual selection.
 */
void gtk_spread_sheet_clipboard_get_selection (GtkSpreadSheetClipBoard *spreadsheetclipboard,
					       GtkSpreadSheetCell **topleft,
					       GtkSpreadSheetCell **bottomright);

/**
 * gtk_spread_sheet_clipboard_get_function:
 * @spreadsheetclipboard: a GtkSpreadSheetClipBoard
 *
 * Returns: The actual function choosed or #NONE. See #GTK_SPREAD_SHEET_CLIPBOARD_FUNC
 * for more informations.
 *
 */
GTK_SPREAD_SHEET_CLIPBOARD_FUNC gtk_spread_sheet_clipboard_get_function (GtkSpreadSheetClipBoard *spreadsheetclipboard);

/**
 * gtk_spread_sheet_clipboard_erase_selection:
 * @spreadsheetclipboard: a GtkSpreadSheetClipBoard
 *
 * Erase the actual selection. Clipboard will be empty after.
 *
 */
void gtk_spread_sheet_clipboard_erase_selection (GtkSpreadSheetClipBoard *spreadsheetclipboard);

/**
 * gtk_spread_sheet_clipboard_set_selection:
 * @spreadsheetclipboard: a GtkSpreadSheetClipBoard
 * @topleft: top/left selected cell
 * @bottomright: bottom/right selected cell
 * @func: function type to this selection (cut or copy)
 *
 * Sets a new selection with @func type.
 *
 * if @func=#NONE and/or if @topleft or @bottomright is #NULL this function will not do anything.
 */
void gtk_spread_sheet_clipboard_set_selection (GtkSpreadSheetClipBoard *spreadsheetclipboard,
					       GtkSpreadSheetCell *topleft,
					       GtkSpreadSheetCell *bottomright,
					       GTK_SPREAD_SHEET_CLIPBOARD_FUNC func);


G_END_DECLS


#endif
