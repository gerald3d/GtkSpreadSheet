#include "GtkSpreadSheetCell.h"

/**
 * SECTION:GtkSpreadSheetCell
 * @Short_description: Defines characteristics of a cell.
 * @Title: GtkSpreadSheetCell
 *
 * inherits of #GObject. Defines characteristics of a cell.
 */

G_DEFINE_TYPE_WITH_PRIVATE (GtkSpreadSheetCell, gtk_spread_sheet_cell, G_TYPE_OBJECT)

static void gtk_spread_sheet_cell_class_init (GtkSpreadSheetCellClass *class);
static void gtk_spread_sheet_cell_init (GtkSpreadSheetCell *cell);
static void gtk_spread_sheet_cell_finalize (GObject * object); // Destructeur

static void
gtk_spread_sheet_cell_class_init (GtkSpreadSheetCellClass *class)
{
  g_return_if_fail (class != NULL);

  G_OBJECT_CLASS(class)->finalize = gtk_spread_sheet_cell_finalize; // destructeur
}

static void
gtk_spread_sheet_cell_init (GtkSpreadSheetCell *cell)
{
  g_return_if_fail (cell != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET_CELL (cell));

  GtkSpreadSheetCellPrivate *priv = gtk_spread_sheet_cell_get_instance_private (cell);

  priv->type_cell = NORMAL;
  priv->bgcolor.red = 1;
  priv->bgcolor.green = 1;
  priv->bgcolor.blue = 1;
  
  priv->shapecolor.red = 0;
  priv->shapecolor.green = 0;
  priv->shapecolor.blue = 0;
  
  priv->textcolor.red = 0;
  priv->textcolor.green = 0;
  priv->textcolor.blue = 0;

  priv->spreadsheetpos.x = 0;
  priv->spreadsheetpos.y = 0;

  priv->graphicposition.x = 0;
  priv->graphicposition.y = 0;

  priv->text = NULL;
}

static void
gtk_spread_sheet_cell_finalize (GObject * object)
{
  GtkSpreadSheetCellPrivate *priv = gtk_spread_sheet_cell_get_instance_private (GTK_SPREAD_SHEET_CELL (object));

  if (priv->text)
    g_free (priv->text);
  priv->text = NULL;

  G_OBJECT_CLASS (gtk_spread_sheet_cell_parent_class)->finalize (object);
}

/******************************************************************************/

GtkSpreadSheetCell*
gtk_spread_sheet_cell_new (TYPE_OF_CELL type_cell, gint x, gint y, const gchar *text)
{
  GtkSpreadSheetCell *cell = NULL;

  g_return_val_if_fail (x>-1, NULL);
  g_return_val_if_fail (y>-1, NULL);

  cell = g_object_new (GTK_TYPE_SPREAD_SHEET_CELL, NULL);
  g_return_val_if_fail (cell!=NULL, NULL);

  GtkSpreadSheetCellPrivate *priv = gtk_spread_sheet_cell_get_instance_private (cell);

  priv->type_cell = type_cell;
  if (text)
    priv->text = g_strdup (text);
  else
    priv->text = g_strdup ("");

  priv->spreadsheetpos.x = x;
  priv->spreadsheetpos.y = y;

  return cell;
}

void
gtk_spread_sheet_cell_set_graphic_position (GtkSpreadSheetCell *cell, gint x, gint y)
{
  g_return_if_fail (x>-1);
  g_return_if_fail (y>-1);
  g_return_if_fail (GTK_IS_SPREAD_SHEET_CELL (cell));

  GtkSpreadSheetCellPrivate *priv = gtk_spread_sheet_cell_get_instance_private (cell);

  priv->graphicposition.x = x;
  priv->graphicposition.y = y;
}

void
gtk_spread_sheet_cell_get_graphic_position (GtkSpreadSheetCell *cell, gint *x, gint *y)
{
  *x=-1; *y=-1;
  g_return_if_fail (GTK_IS_SPREAD_SHEET_CELL (cell));

  GtkSpreadSheetCellPrivate *priv = gtk_spread_sheet_cell_get_instance_private (cell);
  if (x)
    *x = priv->graphicposition.x;
  if (y)
    *y = priv->graphicposition.y;
}

void
gtk_spread_sheet_cell_get_spreadsheet_position (GtkSpreadSheetCell *cell, GdkPoint *position)
{
  position->x=-1; position->y=-1;
  g_return_if_fail (GTK_IS_SPREAD_SHEET_CELL (cell));

  GtkSpreadSheetCellPrivate *priv = gtk_spread_sheet_cell_get_instance_private (cell);

  if (position)
    {
      position->x = priv->spreadsheetpos.x;
      position->y = priv->spreadsheetpos.y;
    }
}

void
gtk_spread_sheet_cell_set_graphic_size (GtkSpreadSheetCell *cell, gint width, gint height)
{
  g_return_if_fail (width>0);
  g_return_if_fail (height>0);
  g_return_if_fail (GTK_IS_SPREAD_SHEET_CELL (cell));

  GtkSpreadSheetCellPrivate *priv = gtk_spread_sheet_cell_get_instance_private (cell);
  
  priv->size.x = width;
  priv->size.y = height;
}

void
gtk_spread_sheet_cell_get_graphic_size (GtkSpreadSheetCell *cell, gint *width, gint *height)
{
  g_return_if_fail (GTK_IS_SPREAD_SHEET_CELL (cell));

  GtkSpreadSheetCellPrivate *priv = gtk_spread_sheet_cell_get_instance_private (cell);

  if (width)
    *width = priv->size.x;
  if (height)
    *height = priv->size.y;
}

void
gtk_spread_sheet_cell_set_text (GtkSpreadSheetCell *cell, const gchar *text)
{
  g_return_if_fail (GTK_IS_SPREAD_SHEET_CELL (cell));

  GtkSpreadSheetCellPrivate *priv = gtk_spread_sheet_cell_get_instance_private (cell);
  
  /* Vérification si l'utilisateur ne cherche pas à modifier la cellule dans le coin haut gauche */
  GdkPoint position;
  gtk_spread_sheet_cell_get_spreadsheet_position (cell, &position);
  if (position.x==0 && position.y==0) return;

  g_free (priv->text);
  if (text)
      priv->text = g_strdup (text);
  else
    priv->text = g_strdup ("");
}

const gchar *
gtk_spread_sheet_cell_get_text (GtkSpreadSheetCell *cell)
{
  g_return_val_if_fail (GTK_IS_SPREAD_SHEET_CELL (cell), NULL);

  GtkSpreadSheetCellPrivate *priv = gtk_spread_sheet_cell_get_instance_private (cell);

  return priv->text;
}

TYPE_OF_CELL
gtk_spread_sheet_cell_get_cell_type (GtkSpreadSheetCell *cell)
{
  g_return_val_if_fail (cell !=NULL, UNDEFINED); 
  g_return_val_if_fail (GTK_IS_SPREAD_SHEET_CELL (cell), UNDEFINED);

  GtkSpreadSheetCellPrivate *priv = gtk_spread_sheet_cell_get_instance_private (cell);

  return priv->type_cell;
}

void
gtk_spread_sheet_cell_get_shapecolor (GtkSpreadSheetCell *cell, ColorCell *color)
{
  g_return_if_fail (GTK_IS_SPREAD_SHEET_CELL (cell));

  GtkSpreadSheetCellPrivate *priv = gtk_spread_sheet_cell_get_instance_private (cell);

  color->red = priv->shapecolor.red;
  color->green = priv->shapecolor.green;
  color->blue = priv->shapecolor.blue;
}

void
gtk_spread_sheet_cell_set_bgcolor (GtkSpreadSheetCell *cell, gdouble red, gdouble green, gdouble blue)
{
  g_return_if_fail (red>-1);
  g_return_if_fail (green>-1);
  g_return_if_fail (blue>-1);
  g_return_if_fail (GTK_IS_SPREAD_SHEET_CELL (cell));

  GtkSpreadSheetCellPrivate *priv = gtk_spread_sheet_cell_get_instance_private (cell);

  priv->bgcolor.red = red;
  priv->bgcolor.green = green;
  priv->bgcolor.blue = blue;
}

void
gtk_spread_sheet_cell_set_textcolor (GtkSpreadSheetCell *cell, gdouble red, gdouble green, gdouble blue)
{
  g_return_if_fail (GTK_IS_SPREAD_SHEET_CELL (cell));

  GtkSpreadSheetCellPrivate *priv = gtk_spread_sheet_cell_get_instance_private (cell);
 
  priv->textcolor.red = red;
  priv->textcolor.green = green;
  priv->textcolor.blue = blue;
}

void
gtk_spread_sheet_cell_paint (GtkSpreadSheetCell *cell, cairo_t *cr, gint posx, gint posy, gboolean REVERSE_COLOR)
{
  g_return_if_fail (posx>-1);
  g_return_if_fail (posy>-1);
  g_return_if_fail (cell !=NULL); 
  g_return_if_fail (GTK_IS_SPREAD_SHEET_CELL (cell));
  g_return_if_fail (cr !=NULL); 

  GtkSpreadSheetCellPrivate *priv = gtk_spread_sheet_cell_get_instance_private (cell);

  switch (priv->type_cell)
    {
    case BORDER :
      {
	cairo_pattern_t *pat;
	pat = cairo_pattern_create_linear (0.0, priv->graphicposition.y,  0.0, priv->graphicposition.y + priv->size.y);
	cairo_pattern_add_color_stop_rgba (pat, 0, 0.6, 0.6, 0.6, 1);
	cairo_pattern_add_color_stop_rgba (pat, 0.5, 1, 1, 1, 1);
	cairo_pattern_add_color_stop_rgba (pat, 1, 0.6, 0.6, 0.6, 1);

	cairo_rectangle (cr, priv->graphicposition.x, priv->graphicposition.y, priv->size.x, priv->size.y);
	cairo_set_source (cr, pat);
	cairo_fill (cr);
	cairo_pattern_destroy (pat);

	/* Affichage du texte si ce n'est pas la première cellule en haut à gauche */
	/* if (posx!=0 && posy!=0) */
	  {
	    cairo_text_extents_t extents;
			    
	    cairo_set_source_rgb (cr, priv->textcolor.red, priv->textcolor.green, priv->textcolor.blue);

	    /* Calcul de la taille en px du texte à afficher et centrage */
	    cairo_set_source_rgb (cr, 0, 0, 0);
	    cairo_text_extents (cr, priv->text, &extents);
	    cairo_move_to (cr, priv->graphicposition.x + (priv->size.x-extents.width)/2,
			   priv->graphicposition.y + (priv->size.y+extents.height)/2);

	    /* Affichage du texte */
	    cairo_show_text (cr, priv->text);
	  }
	break;
      }
    case NORMAL :
      {
	if (!REVERSE_COLOR)
	  {
	    /* Remplissage couleur de fond*/
	    cairo_set_source_rgb (cr, priv->bgcolor.red, priv->bgcolor.green, priv->bgcolor.blue);
	    cairo_rectangle (cr, priv->graphicposition.x, priv->graphicposition.y, priv->size.x, priv->size.y);
	    cairo_fill (cr);

	    /* Ajout du texte */
	    if (priv->text)
	      {
		cairo_text_extents_t extents;

		cairo_set_source_rgb (cr, priv->textcolor.red, priv->textcolor.green, priv->textcolor.blue);

		cairo_text_extents (cr, priv->text, &extents);
		cairo_move_to (cr, priv->graphicposition.x + (priv->size.x-extents.width)/2,
			       priv->graphicposition.y + (priv->size.y+extents.height)/2);
		cairo_show_text (cr, priv->text);
	      }
	  }
	else
	  {
	    /* Remplissage couleur de fond*/
	    cairo_set_source_rgb (cr, 0.5, 0.5, 0.5);
	    cairo_rectangle (cr, priv->graphicposition.x, priv->graphicposition.y, priv->size.x, priv->size.y);
	    cairo_fill (cr);

	    /* Ajout du texte */
	    if (priv->text)
	      {
		cairo_text_extents_t extents;

		cairo_set_source_rgb (cr, 1, 1, 1);

		cairo_text_extents (cr, priv->text, &extents);
		cairo_move_to (cr, priv->graphicposition.x + (priv->size.x-extents.width)/2,
			       priv->graphicposition.y + (priv->size.y+extents.height)/2);
		cairo_show_text (cr, priv->text);
	      }
	  }
      }
    }
}
