#include "GtkSpreadSheetClipBoard.h"

/**
 * SECTION:GtkSpreadSheetClipBoard
 * @Short_description: a simple clipboard for a #GtkSpreadSheet.
 * @Title: GtkSpreadSheetClipBoard
 *
 * inherits of #GObject. Defines an internal clipboard of a #GtkSpreadSheet.
 * Use simply #g_object_unref(); to destroy it.
 *
 * By default a #GtkSpreadSheet contents a clipboard. Normally you dont necessary
 * create it to use.
 * 
 * If you want to use it use #gtk_spread_sheet_get_clipboard(); to retrieve it.
 *
 */

G_DEFINE_TYPE_WITH_PRIVATE (GtkSpreadSheetClipBoard, gtk_spread_sheet_clipboard, G_TYPE_OBJECT)

static void gtk_spread_sheet_clipboard_class_init (GtkSpreadSheetClipBoardClass *class);
static void gtk_spread_sheet_clipboard_init (GtkSpreadSheetClipBoard *spreadsheet);
static void gtk_spread_sheet_clipboard_finalize (GObject * object); // Destructeur

static void
gtk_spread_sheet_clipboard_class_init (GtkSpreadSheetClipBoardClass *class)
{
  g_return_if_fail (class != NULL);

  G_OBJECT_CLASS(class)->finalize = gtk_spread_sheet_clipboard_finalize; // destructeur
}

static void
gtk_spread_sheet_clipboard_init (GtkSpreadSheetClipBoard *spreadsheetclipboard)
{
  g_return_if_fail (spreadsheetclipboard != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET_CLIPBOARD (spreadsheetclipboard));

  GtkSpreadSheetClipBoardPrivate *priv = gtk_spread_sheet_clipboard_get_instance_private (spreadsheetclipboard);

  priv->selecttopleftcell = NULL;
  priv->selectbottomrightcell = NULL;
  priv->func = CLIP_NONE;
}

static void
gtk_spread_sheet_clipboard_finalize (GObject * object)
{
  GtkSpreadSheetClipBoardPrivate *priv = gtk_spread_sheet_clipboard_get_instance_private (GTK_SPREAD_SHEET_CLIPBOARD (object));

  G_OBJECT_CLASS (gtk_spread_sheet_clipboard_parent_class)->finalize (object);
}

/******************************************************************************/


GtkSpreadSheetClipBoard*
gtk_spread_sheet_clipboard_new ()
{
  GtkSpreadSheetClipBoard *spreadsheetclipboard = NULL;

  spreadsheetclipboard = g_object_new (GTK_TYPE_SPREAD_SHEET_CLIPBOARD, NULL);
  g_return_val_if_fail (spreadsheetclipboard!=NULL, NULL);

  return spreadsheetclipboard;
}

void
gtk_spread_sheet_clipboard_get_selection (GtkSpreadSheetClipBoard *spreadsheetclipboard,
					  GtkSpreadSheetCell **topleft,
					  GtkSpreadSheetCell **bottomright)
{
  g_return_if_fail (spreadsheetclipboard!=NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET_CLIPBOARD (spreadsheetclipboard));
  g_return_if_fail (topleft!=NULL);
  g_return_if_fail (bottomright!=NULL);

  GtkSpreadSheetClipBoardPrivate *priv = gtk_spread_sheet_clipboard_get_instance_private (spreadsheetclipboard);

  *topleft = priv->selecttopleftcell;
  *bottomright = priv->selectbottomrightcell;
}

GTK_SPREAD_SHEET_CLIPBOARD_FUNC
gtk_spread_sheet_clipboard_get_function (GtkSpreadSheetClipBoard *spreadsheetclipboard)
{
  g_return_val_if_fail (spreadsheetclipboard != NULL, CLIP_NONE);
  g_return_val_if_fail (GTK_IS_SPREAD_SHEET_CLIPBOARD (spreadsheetclipboard), CLIP_NONE);

  GtkSpreadSheetClipBoardPrivate *priv = gtk_spread_sheet_clipboard_get_instance_private (spreadsheetclipboard);

  return priv->func;
}

void
gtk_spread_sheet_clipboard_erase_selection (GtkSpreadSheetClipBoard *spreadsheetclipboard)
{
  g_return_if_fail (spreadsheetclipboard != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET_CLIPBOARD (spreadsheetclipboard));

  GtkSpreadSheetClipBoardPrivate *priv = gtk_spread_sheet_clipboard_get_instance_private (spreadsheetclipboard);

  priv->selecttopleftcell = NULL;
  priv->selectbottomrightcell = NULL;
  priv->func = CLIP_NONE;
}

void
gtk_spread_sheet_clipboard_set_selection (GtkSpreadSheetClipBoard *spreadsheetclipboard,
					  GtkSpreadSheetCell *topleft,
					  GtkSpreadSheetCell *bottomright,
					  GTK_SPREAD_SHEET_CLIPBOARD_FUNC func)
{
  g_return_if_fail (spreadsheetclipboard != NULL);
  g_return_if_fail (GTK_IS_SPREAD_SHEET_CLIPBOARD (spreadsheetclipboard));
  g_return_if_fail (func!=CLIP_NONE);
  g_return_if_fail (topleft!=NULL);
  g_return_if_fail (bottomright!=NULL);

  GtkSpreadSheetClipBoardPrivate *priv = gtk_spread_sheet_clipboard_get_instance_private (spreadsheetclipboard);

  priv->selecttopleftcell = topleft;
  priv->selectbottomrightcell = bottomright;
  priv->func = func;
}
