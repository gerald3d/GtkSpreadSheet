/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * GtkSpreadSheetCell.h
 * Copyright (C) 2018 Gerald Dumas <gerald.dumas@laposte.net>
 * 
 * GtkSpreadSheetCell is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * GtkSpreadSheetCell is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GTK_SPREAD_SHEET_CELL_H__
#define __GTK_SPREAD_SHEET_CELL_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GTK_TYPE_SPREAD_SHEET_CELL            (gtk_spread_sheet_cell_get_type ())
#define GTK_SPREAD_SHEET_CELL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_SPREAD_SHEET_CELL, GtkSpreadSheetCell))
#define GTK_SPREAD_SHEET_CELL_CLASS(class)    (G_TYPE_CHECK_CLASS_CAST ((class), GTK_TYPE_SPREAD_SHEET_CELL, GtkSpreadSheetCellClass))
#define GTK_IS_SPREAD_SHEET_CELL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_SPREAD_SHEET_CELL))
#define GTK_IS_SPREAD_SHEET_CELL_CLASS(class) (G_TYPE_CHECK_CLASS_TYPE ((class), GTK_TYPE_SPREAD_SHEET_CELL))
#define GTK_SPREAD_SHEET_CELL_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GTK_TYPE_SPREAD_SHEET_CELL, GtkSpreadSheetCellClass))
typedef struct _GtkSpreadSheetCell GtkSpreadSheetCell;
typedef struct _GtkSpreadSheetCellClass GtkSpreadSheetCellClass;
typedef struct _GtkSpreadSheetCellPrivate GtkSpreadSheetCellPrivate;
typedef struct _ColorCell ColorCell;

/**
 * GtkSpreadSheetCell:
 *
 * All the fields in the #GtkSpreadSheetCell structure are private to the #GtkSpreadSheetCell
 * implementation and should never be accessed directly.
 */
struct _GtkSpreadSheetCell {
  /*< private >*/
  GObject parent;

  GtkSpreadSheetCellPrivate *priv;
};

/**
 * ColorCell:
 * @red : red component
 * @green : green component
 * @blue : blue component
 *
 * Each color component can take a value between 0 and 1.
 */
struct _ColorCell
{
  gdouble red;
  gdouble green;
  gdouble blue;

};

/**
 * TYPE_OF_CELL:
 * @UNDEFINED : no defined
 * @BORDER : type of irst column or row cell
 * @NORMAL : anothers cells
 *
 */
typedef enum {UNDEFINED, BORDER, NORMAL} TYPE_OF_CELL;

/**
 * GtkSpreadSheetCellPrivate:
 *
 * All the fields in the #GtkSpreadSheetCellPrivate structure are private to the
 * #GtkSpreadSheetCellPrivate implementation and should never be accessed directly.
 */
struct _GtkSpreadSheetCellPrivate
{
  /*< private >*/
  TYPE_OF_CELL type_cell;     // Type de cellule
  GdkPoint graphicposition;   // angle haut gauche de la cellule coordonnées graphiques
  GdkPoint size;              // taille de la cellule
  ColorCell bgcolor;          // couleur de fond de la cellule
  ColorCell shapecolor;       // couleur du contour de la cellule
  ColorCell textcolor;        // couleur du texte inséré dans la cellule
  gchar *text;                // texte inséré dans la cellule

  GdkPoint spreadsheetpos;    // Position de la cellule dans le tableur
};

/**
 * GtkSpreadSheetCellClass:
 *
 * All the fields in the #GtkSpreadSheetCellClass structure are private to the
 * #GtkSpreadSheetCellClass implementation and should never be accessed directly.
 */
struct _GtkSpreadSheetCellClass
{
  /*< private >*/
   GObjectClass parent_class;
};

GType gtk_spread_sheet_cell_get_type ();

/**
 * gtk_spread_sheet_cell_new:
 * @type_cell: type of cell
 * @x: x coordinate in spread sheet
 * @y: y coordinate in spread sheet
 * @text: text
 *
 * Create a new cell.
 *
 * Returns: a #GtkSpreadSheetCell.
 *
 */
GtkSpreadSheetCell* gtk_spread_sheet_cell_new (TYPE_OF_CELL type_cell, gint x, gint y, const gchar *text);

/**
 * gtk_spread_sheet_cell_set_graphic_position:
 * @cell: a GtkSpreadSheetCell
 * @x: x graphic coordinate top/left of the cell
 * @y: y graphic coordinate top/left of the cell
 *
 * Set the graphic position of the cell.
 *
 * This function should not be used by the final user.
 *
 */
void gtk_spread_sheet_cell_set_graphic_position (GtkSpreadSheetCell *cell, gint x, gint y);

/**
 * gtk_spread_sheet_cell_get_graphic_position:
 * @cell: a GtkSpreadSheetCell
 * @x: x graphic coordinate top/left of the cell
 * @y: y graphic coordinate top/left of the cell
 *
 * Returns: The graphic position top/left of the cell. Returns -1 in @x and @y in case of failure.
 *
 */
void gtk_spread_sheet_cell_get_graphic_position (GtkSpreadSheetCell *cell, gint *x, gint *y);

/**
 * gtk_spread_sheet_cell_get_spreadsheet_position:
 * @cell: a GtkSpreadSheetCell
 * @position: cell spread sheet coordinates
 *
 * Returns: The spread sheet position. Returns -1 in @x and @y in case of failure.
 *
 */
void gtk_spread_sheet_cell_get_spreadsheet_position (GtkSpreadSheetCell *cell, GdkPoint *position);

/**
 * gtk_spread_sheet_cell_set_graphic_size:
 * @cell: a GtkSpreadSheetCell
 * @width: width (in pixels) of the cell
 * @height: height (in pixel) of the cell
 *
 * Sets the graphic size of the cell.
 *
 */
void gtk_spread_sheet_cell_set_graphic_size (GtkSpreadSheetCell *cell, gint width, gint height);

/**
 * gtk_spread_sheet_cell_get_graphic_size:
 * @cell: a GtkSpreadSheetCell
 * @width: width (in pixels) of the cell
 * @height: height (in pixel) of the cell
 *
 * Returns in @width and @height the graphic size of the cell.
 *
 */
void gtk_spread_sheet_cell_get_graphic_size (GtkSpreadSheetCell *cell, gint *width, gint *height);

/**
 * gtk_spread_sheet_cell_set_text:
 * @cell: a GtkSpreadSheetCell
 * @text: text
 *
 * Sets the text in the cell.
 *
 */
void gtk_spread_sheet_cell_set_text (GtkSpreadSheetCell *cell, const gchar *text);

/**
 * gtk_spread_sheet_cell_get_text:
 * @cell: a GtkSpreadSheetCell
 * @text: text
 *
 * Returns: The text in the cell.
 *
 */
const gchar *gtk_spread_sheet_cell_get_text (GtkSpreadSheetCell *cell);

/**
 * gtk_spread_sheet_cell_get_shapecolor:
 * @cell: a GtkSpreadSheetCell
 * @color: border color of the cell
 *
 * Returns in @color the border color of the cell.
 *
 */
void gtk_spread_sheet_cell_get_shapecolor (GtkSpreadSheetCell *cell, ColorCell *color);

/**
 * gtk_spread_sheet_cell_set_bgcolor:
 * @cell: a GtkSpreadSheetCell
 * @red: red component
 * @green: green component
 * @blue: blue component
 *
 * sets the background color of the cell.
 *
 * Each color component can take a value between 0 and 1.
 *
 */
void gtk_spread_sheet_cell_set_bgcolor (GtkSpreadSheetCell *cell, gdouble red, gdouble green, gdouble blue);

/**
 * gtk_spread_sheet_cell_set_textcolor:
 * @cell: a GtkSpreadSheetCell
 * @red: red component
 * @green: green component
 * @blue: blue component
 *
 * Sets the text color of the cell.
 *
 * Each color component can take a value between 0 and 1.
 *
 */
void gtk_spread_sheet_cell_set_textcolor (GtkSpreadSheetCell *cell, gdouble red, gdouble green, gdouble blue);

/**
 * gtk_spread_sheet_cell_get_cell_type:
 * @cell: a GtkSpreadSheetCell
 *
 * Returns: The type of the cell. See #TYPE_OF_CELL to know the differents types. Returns #UNDEFINED in case of failure.
 *
 */
TYPE_OF_CELL gtk_spread_sheet_cell_get_cell_type (GtkSpreadSheetCell *cell);

/* Changement des caratéristiques de la font du texte */
/* void gtk_spread_sheet_cell_set_font_face (GtkSpreadSheetCell *cell, cairo_font_face_t *font_face); */

/* Dessin de la cellule dans le contexte transmis
 * cr : contexte dans lequel est dessinée la cellule
 * posx, posy : coordoonées de la cellule dans le tableur
*/
/**
 * gtk_spread_sheet_cell_paint:
 * @cell: a GtkSpreadSheetCell
 * @cr: cairo_context
 * @posx: x graphic coordinate of top/left
 * @posy: y graphic coordinate of top/left
 * @REVERSE_COLOR: #TRUE if the cell is selected
 *
 * This function should not be used by the final user.
 *
 */
void gtk_spread_sheet_cell_paint (GtkSpreadSheetCell *cell, cairo_t *cr, gint posx, gint posy, gboolean REVERSE_COLOR);

G_END_DECLS


#endif
