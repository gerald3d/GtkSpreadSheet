/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * GtkSpreadSheetEntry.h
 * Copyright (C) 2018 Gerald Dumas <gerald.dumas@laposte.net>
 * 
 * GtkSpreadSheetEntry is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * GtkSpreadSheetEntry is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtk/gtk.h>
#include "GtkSpreadSheet.h"
#include "GtkSpreadSheetCell.h"

/* Création d'une fenêtre principale sans décors avec un GtkEntry.
 * Tous les signaux du GtkEntry sont gérés ici.
 * posx, posy : coordonnées tableur de la cellule éditée.
 */
GtkWidget *gtk_spread_sheet_entry_new (GtkSpreadSheet *spreadsheet, GtkSpreadSheetCell *cell);
