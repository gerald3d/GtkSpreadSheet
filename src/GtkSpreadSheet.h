/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * GtkSpreadSheet.h
 * Copyright (C) 2018 Gerald Dumas <gerald.dumas@laposte.net>
 * 
 * GtkSpreadSheet is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * GtkSpreadSheet is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GTK_SPREAD_SHEET_H__
#define __GTK_SPREAD_SHEET_H__

#include <gtk/gtk.h>
#include <string.h>
#include "GtkSpreadSheetCell.h"
#include "gdkkeysyms.h"
#include "GtkSpreadSheetClipBoard.h"
#include "GtkSpreadSheetMarshal.h"

G_BEGIN_DECLS

#define GTK_TYPE_SPREAD_SHEET            (gtk_spread_sheet_get_type ())
#define GTK_SPREAD_SHEET(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_SPREAD_SHEET, GtkSpreadSheet))
#define GTK_SPREAD_SHEET_CLASS(class)    (G_TYPE_CHECK_CLASS_CAST ((class), GTK_TYPE_SPREAD_SHEET, GtkSpreadSheetClass))
#define GTK_IS_SPREAD_SHEET(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_SPREAD_SHEET))
#define GTK_IS_SPREAD_SHEET_CLASS(class) (G_TYPE_CHECK_CLASS_TYPE ((class), GTK_TYPE_SPREAD_SHEET))
#define GTK_SPREAD_SHEET_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GTK_TYPE_SPREAD_SHEET, GtkSpreadSheetClass))
typedef struct _GtkSpreadSheet GtkSpreadSheet;
typedef struct _GtkSpreadSheetClass GtkSpreadSheetClass;
typedef struct _GtkSpreadSheetPrivate GtkSpreadSheetPrivate;

GType gtk_spread_sheet_get_type ();

/**
 * TYPE_OF_MOVE:
 * @NONE : No selection
 * @COLUMNS : selected column(s)
 * @ROWS: selected row(s)
 *
 */
typedef enum {NONE, COLUMNS, ROWS} TYPE_OF_MOVE;

/**
 * TYPE_OF_INCREMENT:
 * @USER_INCREMENT : columns and rows labels are defined by user
 * @AUTO_INCREMENT : columns and rows are automaticaly increment
 *
 * By default #AUTO_INCREMENT are used. If you insert columns or rows
 * all labels will be defined.
 *
 * If you define #USER_INCREMENT with #gtk_spread_sheet_set_labels_increment();,
 * when you insert columns or rows, the new label will be empty.
 *
 *Use #gtk_spread_sheet_set_column_label(); or #gtk_spread_sheet_set_row_label();
 * to define it.
 */
typedef enum {USER_INCREMENT, AUTO_INCREMENT} TYPE_OF_INCREMENT;

/**
 * GTK_SPREAD_SHEET_MOVE:
 * @GTK_SPREAD_SELECT_MOVE_LEFT: move to left
 * @GTK_SPREAD_SELECT_MOVE_RIGHT: move to right
 * @GTK_SPREAD_SELECT_MOVE_UP: move to up
 * @GTK_SPREAD_SELECT_MOVE_DOWN: move to down
 *
 * Used with gtk_spread_sheet_set_direction_of_selected_cell (); and #gtk_spread_sheet_get_direction_of_selected_cell ();
 */
typedef enum {GTK_SPREAD_SELECT_MOVE_LEFT, GTK_SPREAD_SELECT_MOVE_RIGHT, GTK_SPREAD_SELECT_MOVE_UP, GTK_SPREAD_SELECT_MOVE_DOWN} GTK_SPREAD_SHEET_MOVE;

enum {
  GTK_SPREAD_SHEET_CHANGED_SIGNAL,
  GTK_SPREAD_SHEET_SELECT_COLUMN_SIGNAL,
  GTK_SPREAD_SHEET_SELECT_ROW_SIGNAL,
  GTK_SPREAD_SHEET_SELECT_RANGE_SIGNAL,
  GTK_SPREAD_SHEET_SELECT_MOVE_SIGNAL,
  GTK_SPREAD_SHEET_NB_SIGNALS
};

/**
 * GtkSpreadSheet:
 *
 * All the fields in the #GtkSpreadSheet structure are private to the #GtkSpreadSheet
 * implementation and should never be accessed directly.
 */
struct _GtkSpreadSheet {
  /*< private >*/
  GtkDrawingArea parent;

  GtkSpreadSheetPrivate *priv;
};

/**
 * GtkSpreadSheetPrivate:
 *
 * All the fields in the #GtkSpreadSheetPrivate structure are private to the #GtkSpreadSheetPrivate
 * implementation and should never be accessed directly.
 */
struct _GtkSpreadSheetPrivate
{
  /*< private >*/
  gint nb_rows, nb_cols;                // Nbre de colonnes et de lignes du tableur
  GHashTable *tab;                      // Tableau contenant tout le tableur
  GdkPoint activecell;                  // Coordonnées de la cellule actuellement pointée
  GdkCursor *colcursor;                 // Image de la souris lors du redimensionnement des colonnes
  GdkCursor *rowcursor;                 // Image de la souris lors du redimensionnement des lignes
  TYPE_OF_MOVE TYPE_REDIM;              // Type de redimensionnement en cours (horizontal ou vertical)
  TYPE_OF_INCREMENT increment;          // type d'incrément des labels lors d'une insertion

  gboolean COL_ROW_RESIZE;              // drapeau pour savoir si le bouton est déjà pressé
  gboolean MULTIPLE_SELECT;             // drapeau pour savoir si on est en cours de sélection multiple

  GtkWidget *rootwindow;                // Widget toplevel dans lequel est inséré le tableur. Permet de
                                        // mettre à jour en temps réel la position du GtkEntry
  GdkPoint rootwindowposition;          // Coin haut gauche de la fenêtre toplevel
  GtkWidget *winentry;                  // Fenêtre popup pour l'édition d'une cellule
  GtkSpreadSheetCell *lastcellchanged;  // Dernière cellule modifiée par l'utilisateur
  GtkSpreadSheetCell *cellunderpointer; // Cellule actuellement pointée par la souris

  /* Données de gestion de la sélection multiple */
  gint emitted_signal;                  // type de signal emis
  GtkSpreadSheetCell *selecttopleftcell;
  GtkSpreadSheetCell *selectbottomrightcell;

  /* Clipboard interne */
  GtkSpreadSheetClipBoard *clipboard;

  /* Menu contextuel */
  GtkWidget *contextmenu;
  GtkWidget *insertcolitembefore;
  GtkWidget *insertrowitembefore;
  GtkWidget *insertcolitemafter;
  GtkWidget *insertrowitemafter;
  GtkWidget *removecolitem;
  GtkWidget *removerowitem;
  GtkWidget *cutitem;
  GtkWidget *copyitem;
  GtkWidget *pasteitem;

  GTK_SPREAD_SHEET_MOVE selected_enter_move; // direction de la cellule suivante en tapant sur "Entrer"
};

/**
 * GtkSpreadSheetClass:
 * @changed : signal emitted when a value changed in spread sheet.
 * @select_column : signal emitted when one or more columns are selected.
 * @select_row : signal emitted when one or more rows are selected.
 * @select_range : signal emitted when selected cells are done.
 * @select_move : signal emitted when selected cells move with keyboard.
 */
struct _GtkSpreadSheetClass
{
  /*< private >*/
   GtkDrawingAreaClass parent_class;

  /* Fonctions virtuelles surchargeables */
  /*< public >*/
  void (*changed) (GtkSpreadSheet *spreadsheet,
                   GtkSpreadSheetCell *cell, gpointer user_data);

  void (*select_column) (GtkSpreadSheet *spreadsheet,
                         GtkSpreadSheetCell *topleftcell,
                         GtkSpreadSheetCell *bottomrightcell, gpointer user_data);

  void (*select_row) (GtkSpreadSheet *spreadsheet,
                      GtkSpreadSheetCell *topleftcell,
                      GtkSpreadSheetCell *bottomrightcell, gpointer user_data);

  void (*select_range) (GtkSpreadSheet *spreadsheet,
                        GtkSpreadSheetCell *topleftcell,
                        GtkSpreadSheetCell *bottomrightcell, gpointer user_data);

  void (*select_move) (GtkSpreadSheet *spreadsheet,
                       GtkSpreadSheetCell *topleftcell,
                       GtkSpreadSheetCell *bottomrightcell, gpointer user_data);
};

#include "GtkSpreadSheetEntry.h"

/**
 * gtk_spread_sheet_new:
 * @width: number of columns
 * @height: number of rows
 *
 * Create a new spread sheet with size (@width, @height). @width and @height
 * must be greater than 0.
 *
 * Returns: a new #GtkSpreadSheet.
 *
 */
GtkWidget* gtk_spread_sheet_new (gint width, gint height);

/**
 * gtk_spread_sheet_get_cell:
 * @spreadsheet: a GtkSpreadSheet
 * @x: the x coordinate of the cell (x>0)
 * @y: the y coordinate of the cell (y>0)
 *
 * Returns: the #GtkSpreadSheetCell of the cell (@x, @y) or #NULL in case of failure.
 *
 */
GtkSpreadSheetCell *gtk_spread_sheet_get_cell (GtkSpreadSheet *spreadsheet, gint x, gint y);


/**
 * gtk_spread_sheet_insert_col_row:
 * @spreadsheet: a GtkSpreadSheet
 * @num: column number or row number where insert a new column/row
 * @colrow: type of insertion : #COLUMNS or #ROWS
 * @after: insert after or before the selected column/row
 *
 * Inserts a column or a row before or after the column/row relative to @num.
 *
 * if @colrow is different from #COLUMNS or #ROWS or/and if @num is not a valid column/row
 * this function does nothing.
 *
 * It's possible to insert a new column/row after the last column/row. if @num
 * more than superior to the last column/row+1, then this function create just
 * a new column/row at the end.
 *
 */
void gtk_spread_sheet_insert_col_row (GtkSpreadSheet *spreadsheet, gint num, TYPE_OF_MOVE colrow, gboolean after);

/**
 * gtk_spread_sheet_remove_col_row:
 * @spreadsheet: a GtkSpreadSheet
 * @num: column number or row number which must be deleted
 * @colrow: type of delete : #COLUMNS ou #ROWS
 *
 * Deletes a column or a row relative to @num.
 *
 * If @colrow is different from #COLUMNS or #ROWS this function does not do anything.
 * If num <= 0 or num > size of spreadsheet, this function does not do anything too.
 *
 */
void gtk_spread_sheet_remove_col_row (GtkSpreadSheet *spreadsheet, gint num, TYPE_OF_MOVE colrow);

/**
 * gtk_spread_sheet_get_size:
 * @spreadsheet: a GtkSpreadSheet
 * @cols: number of columns
 * @rows: number of rows
 *
 * Returns: @cols et@rows have the number of columns and rows. Returns -1 in case of failure.
 *
 */
void gtk_spread_sheet_get_size (GtkSpreadSheet *spreadsheet, gint *cols, gint *rows);

/**
 * gtk_spread_sheet_resize:
 * @spreadsheet: a GtkSpreadSheet
 * @cols: number of columns
 * @rows: number of rows
 *
 * Set the new size of spread sheet.
 *
 * If @cols or @rows < 1 the new size will be (1, @rows) or (@cols, 1).
 *
 */
void gtk_spread_sheet_resize (GtkSpreadSheet *spreadsheet, gint cols, gint rows);

/**
 * gtk_spread_sheet_auto_resize_graphic_cell:
 * @spreadsheet: a GtkSpreadSheet
 *
 * Sets the correct graphic size (in pixels) for each column and row.
 *
 */
void gtk_spread_sheet_auto_resize_graphic_cell (GtkSpreadSheet *spreadsheet);

/**
 * gtk_spread_sheet_get_last_cell_changed:
 * @spreadsheet: a GtkSpreadSheet
 *
 *
 * Returns: The last modified cell or #NULL in case of failure.
 *
 */
GtkSpreadSheetCell *gtk_spread_sheet_get_last_cell_changed (GtkSpreadSheet *spreadsheet);

/**
 * gtk_spread_sheet_set_change_cell:
 * @spreadsheet: a GtkSpreadSheet
 * @cell: cell to modify
 *
 * Save the cell to modify.
 *
 * This function should not be used by the final user.
 *
 */
void gtk_spread_sheet_set_change_cell (GtkSpreadSheet *spreadsheet, GtkSpreadSheetCell *cell);

/**
 * gtk_spread_sheet_set_selected:
 * @spreadsheet: a GtkSpreadSheet
 * @first: cell coordinates in an angle of the selection
 * @last: cell coordinates in an opposite corner of the selection
 * 
 * Selects one or more cells. The screen is automatically updated.
 *
 * The selection is automatically reorder. @first will be the top/left corner
 * and @last the bottom/right corner.
 *
 * This function has no effect in case of failure.
 *
 */
void gtk_spread_sheet_set_selected (GtkSpreadSheet *spreadsheet, GdkPoint first, GdkPoint last);

/**
 * gtk_spread_sheet_get_selected:
 * @spreadsheet: a GtkSpreadSheet
 * @topleft : cell coordinates of the top/left corner of the selection
 * @bottomright: cell coordinates of the bottom/right corner of the selection
 *
 * Returns: The actual selection in @topleft and @bottomright.
 *
 * In case of failure this function returns #FALSE or if there is no selection. In this case
 * @topleft coordinates and @bottomright coordinates return -1.
 *
 * If @topleft or @bottomright are #NULL returns #FALSE again.
 *
 */
gboolean gtk_spread_sheet_get_selected (GtkSpreadSheet *spreadsheet, GdkPoint *topleft, GdkPoint *bottomright);

/**
 * gtk_spread_sheet_deselect:
 * @spreadsheet: a GtkSpreadSheet
 *
 * Deletes the actual selection. The screen is automatically updated.
 *
 */
void gtk_spread_sheet_deselect (GtkSpreadSheet *spreadsheet);

/**
 * gtk_spread_sheet_set_multiple_selected_colors:
 * @spreadsheet: a GtkSpreadSheet
 * @bgcolor: background color of the cell
 * @textcolor: text color of the cell
 *
 * Sets the background color and text color of the selected cells.
 *
 * The screen is automatically updated.
 *
 */
void gtk_spread_sheet_set_multiple_selected_colors (GtkSpreadSheet *spreadsheet, ColorCell *bgcolor, ColorCell *textcolor);

/**
 * gtk_spread_sheet_set_direction_of_selected_cell:
 * @spreadsheet: a GtkSpreadSheet
 * @move: direction to select the next cell after a cell validation
 *
 * When you press the "Enter" key the next selected cell will be selected with the @move direction.
 * It's possible to choose #GTK_SPREAD_SELECT_MOVE_LEFT, #GTK_SPREAD_SELECT_MOVE_RIGHT, #GTK_SPREAD_SELECT_MOVE_UP
 * or #GTK_SPREAD_SELECT_MOVE_DOWN.
 *
 * By default the #GTK_SPREAD_SELECT_MOVE_RIGHT direction is selected.
 *
 */
void gtk_spread_sheet_set_direction_of_selected_cell (GtkSpreadSheet *spreadsheet, GTK_SPREAD_SHEET_MOVE move);

/**
 * gtk_spread_sheet_get_direction_of_selected_cell:
 * @spreadsheet: a GtkSpreadSheet
 *
 * Returns: The type of direction to the next selection. See #gtk_spread_sheet_set_direction_of_selected_cell ();
 * for more information.
 *
 * Returns #UNDEFINED in case of failure.
 *
 */
GTK_SPREAD_SHEET_MOVE gtk_spread_sheet_get_direction_of_selected_cell (GtkSpreadSheet *spreadsheet);

/**
 * gtk_spread_sheet_set_labels_increment:
 * @spreadsheet: a GtkSpreadSheet
 * @increment : #AUTO_INCREMENT or #USER_INCREMENT
 *
 * Define how labels are defined when inserting columns or rows.
 *
 * By default #AUTO_INCREMENT is set.
 *
 */
void gtk_spread_sheet_set_labels_increment (GtkSpreadSheet *spreadsheet, TYPE_OF_INCREMENT increment);

/**
 * gtk_spread_sheet_get_labels_increment:
 * @spreadsheet: a GtkSpreadSheet
 *
 * Returns the type of increment used.
 *
 */
TYPE_OF_INCREMENT gtk_spread_sheet_get_labels_increment (GtkSpreadSheet *spreadsheet);

/**
 * gtk_spread_sheet_set_column_label:
 * @spreadsheet: a GtkSpreadSheet
 * @column : column number
 * @label : text
 *
 * Sets text for the first cell of the @column.
 *
 * Returns: #AUTO_INCREMENT or #USER_INCREMENT. #UNDEFINED in case of failure.
 *
 */
void gtk_spread_sheet_set_column_label (GtkSpreadSheet *spreadsheet, gint column, const gchar *label);

/**
 * gtk_spread_sheet_set_row_label:
 * @spreadsheet: a GtkSpreadSheet
 * @row : row number
 * @label : text
 *
 * Sets text for the first cell of the @row.
 *
 */
void gtk_spread_sheet_set_row_label (GtkSpreadSheet *spreadsheet, gint row, const gchar *label);

/**
 * gtk_spread_sheet_set_origin_column_number:
 * @spreadsheet: a GtkSpreadSheet
 * @origin : starting number
 *
 * (re)number columns with @orign as the starting number.
 *
 * @origin may be negative.
 *
 */
void gtk_spread_sheet_set_origin_column_number (GtkSpreadSheet *spreadsheet, gint origin);

/**
 * gtk_spread_sheet_set_origin_row_number:
 * @spreadsheet: a GtkSpreadSheet
 * @origin : starting number
 *
 * (re)number rows with @orign as the starting number.
 *
 * @origin may be negative.
 *
 */
void gtk_spread_sheet_set_origin_row_number (GtkSpreadSheet *spreadsheet, gint origin);

/**
 * gtk_spread_sheet_get_entry:
 * @spreadsheet: a GtkSpreadSheet
 *
 * Returns: the actually #GtkSpreadSheetEntry showned or #NULL in case of failure.
 *
 */
GtkWidget *gtk_spread_sheet_get_entry (GtkSpreadSheet *spreadsheet);

/**
 * gtk_spread_sheet_get_toplevel_window:
 * @spreadsheet: a GtkSpreadSheet
 *
 * Returns: The #GTK_WINDOW_TOPLEVEL window of the @GtkSpreadSheet.
 *
 */
GtkWidget *gtk_spread_sheet_get_toplevel_window (GtkSpreadSheet *spreadsheet);

/**
 * gtk_spread_sheet_get_clipboard:
 * @spreadsheet: a GtkSpreadSheet
 *
 * Returns: The #GtkSpreadSheetClipBoard or #NULL in case of failure.
 *
 */
GtkSpreadSheetClipBoard *gtk_spread_sheet_get_clipboard (GtkSpreadSheet *spreadsheet);

/**
 * gtk_spread_sheet_clipboard_selection_paste:
 * @spreadsheet: a GtkSpreadSheet
 * @destination: destination cell
 * @overwrite: overwrite the contents of destination cells
 *
 * Paste the actually selection to @destination cell.
 *
 * Returns: TRUE in success. If the place of destination is insufficient
 * or if the destination is not empty and @overwrite = FALSE the function will return FALSE.
 *
 */
gboolean gtk_spread_sheet_clipboard_selection_paste (GtkSpreadSheet *spreadsheet,
                                                     GtkSpreadSheetCell *destination,
                                                     gboolean overwrite);

G_END_DECLS


#endif
